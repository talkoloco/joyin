
#import "HintView.h"
#import "UIView+SEAnimations.h"

#import <QuartzCore/QuartzCore.h>

@interface HintView()

@property (nonatomic,retain) UIImageView* imageViewTitleIcon;
@property (nonatomic,retain) UILabel* labelTitle;
@property (nonatomic,retain) UIButton* buttonDismiss;
@property (nonatomic,retain) UIPageControl* pageControl;
@property (nonatomic,retain) UIScrollView* scrollViewPages;
@property (nonatomic,retain) NSTimer* dismissTimer;
//@property (atomic,assign) BOOL isDismissing;

@end

@implementation HintView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
        self.clipsToBounds = YES;
        self.spanWidthWeight = 1.0f;
        //self.layer.cornerRadius = 12.0;
        //self.maximumHeight = 50;
        self.userInteractionEnabled = YES;
        self.presentationAnimation = hintViewPresentationSlide;
      
    }
    
    return self;
}

-(void) show:(NSTimeInterval)time
{
    self.dismissTimer = [NSTimer scheduledTimerWithTimeInterval:time target:self selector:@selector(dismiss) userInfo:nil repeats:NO];
    
    [self show];
}


-(void) show
{
    [self createSubviews];
    
    CGRect outerFrame = self.superview.frame;
    UIView* temp = [[UIView alloc] initWithFrame:outerFrame];
        
    if( self.presentationAnimation == hintViewPresentationSlide )
    {
        [self animationSlideInWithDirection:kSEAnimationTop boundaryView:temp duration:0.3];
    }
    else if( self.presentationAnimation == hintViewPresentationFade )
    {
        [self animationFadeInWithDuration:0.3];
    }
    else if( self.presentationAnimation == hintViewPresentationDrop )
    {   
        [self animationDropInWithDuration:0.3];
    }
    else if( self.presentationAnimation == hintViewPresentationSwirl )
    {
        [self animationSwirlInWithDuration:0.7];
    }
    else if( self.presentationAnimation == hintViewPresentationBounce )
    {
        [self animationBounceInWithDirection:kSEAnimationTop boundaryView:temp duration:0.3];
    }
}



-(void) dismiss
{
    /*if( self.isDismissing )
    {
        return;
    }
    
    self.isDismissing = YES;*/
    
    if( self.dismissTimer )
    {
        [self.dismissTimer invalidate];
    }
    

    if( self.presentationAnimation == hintViewPresentationSlide )
    {
        [self animationSlideOutWithDirection:kSEAnimationTop boundaryView:self.superview duration:0.3 delegate:self startSelector:nil stopSelector:@selector(dismissAnimationCompleted)];
    }
    else if( self.presentationAnimation == hintViewPresentationFade )
    {
        [self animationFadeOutWithDuration:0.3 delegate:self startSelector:nil stopSelector:@selector(dismissAnimationCompleted)];
    }
    else if( self.presentationAnimation == hintViewPresentationDrop )
    {
        [self animationDropOutWithDuration:0.3 delegate:self startSelector:nil stopSelector:@selector(dismissAnimationCompleted)];
    }
    else if( self.presentationAnimation == hintViewPresentationSwirl )
    {
        [self animationSwirlOutWithDuration:0.7 delegate:self startSelector:nil stopSelector:@selector(dismissAnimationCompleted)];
    }
    else if( self.presentationAnimation == hintViewPresentationBounce )
    {
        [self animationBounceOutWithDirection:kSEAnimationTop boundaryView:self.superview duration:0.3 delegate:self startSelector:nil stopSelector:@selector(dismissAnimationCompleted)];
    }
}

-(void) dismissAnimationCompleted
{
    if( self.delegate && [self.delegate respondsToSelector:@selector(dismissedHintView:)] )
    {
        [self.delegate dismissedHintView:self];
    }
    
    [self removeFromSuperview];
}


-(void) createSubviews
{
    self.imageViewTitleIcon.hidden = NO;
    self.labelTitle.hidden = NO;
    self.buttonDismiss.hidden = NO;
    self.pageControl.hidden = NO;
    self.scrollViewPages.hidden = NO;
}

-(UIButton *)buttonDismiss
{
    if( !_buttonDismiss )
    {
        _buttonDismiss = [[UIButton alloc] initWithFrame:CGRectMake( self.frame.size.width - 70, self.frame.size.height/2 - 32, 64, 64)];
        _buttonDismiss.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin;
        
        [_buttonDismiss setImage:[UIImage imageNamed:@"60-x"] forState:UIControlStateNormal];
        [_buttonDismiss addTarget:self action:@selector(dismiss) forControlEvents:UIControlEventTouchUpInside];
        
        [self addSubview:_buttonDismiss];
    }
    
    return _buttonDismiss;
}


-(UILabel *)labelTitle
{
    if( !_labelTitle )
    {
        _labelTitle = [[UILabel alloc] initWithFrame:CGRectMake(20, self.frame.size.height/2 - 30, self.frame.size.width - 80, 60)];
        _labelTitle.textColor = [UIColor whiteColor];
		_labelTitle.backgroundColor = [UIColor clearColor];
        _labelTitle.font = [UIFont boldSystemFontOfSize:14.0];
        _labelTitle.numberOfLines = 2;
        
		[self addSubview:_labelTitle];
    }
    
    if( self.dataSource && [self.dataSource respondsToSelector:@selector(titleforHintView:)] )
    {
        _labelTitle.text = [self.dataSource titleforHintView:self];
    }
    
    return _labelTitle;
}

@end
