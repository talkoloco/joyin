#import <UIKit/UIKit.h>

typedef enum
{
    hintViewOrientationTop,
    hintViewOrientationBottom
    
} HintViewOrientation;


typedef enum
{
    hintViewPresentationFade,
    hintViewPresentationDrop,
    hintViewPresentationSwirl,
    hintViewPresentationSlide,
    hintViewPresentationBounce
    
} HintViewPresentationAnimation;


@protocol HintViewDataSource;
@protocol HintViewDelegate;

@interface HintView : UIView<UIScrollViewDelegate>

@property (nonatomic,strong) id<HintViewDataSource> dataSource;
@property (nonatomic,strong) id<HintViewDelegate> delegate;

@property (nonatomic,strong) UIImage* backgroundImage;
@property (nonatomic,strong) UIColor* textColor;
@property (nonatomic,assign) CGFloat spanWidthWeight; // Width weight between 0.01f and 1.0f
@property (nonatomic,assign) HintViewPresentationAnimation presentationAnimation;
@property (nonatomic,assign) HintViewOrientation orientation;
@property (nonatomic,assign) CGFloat maximumHeight;
@property (nonatomic,assign) NSUInteger type;

-(void) show;
-(void) show:(NSTimeInterval)duration;
-(void) dismiss;

@end

@protocol HintViewDataSource<NSObject>
@optional
-(NSInteger) numberOfPagesInHintView:(HintView*)hintView;
-(UIView*) viewForPage:(NSUInteger)page hintView:(HintView*)hintView;
-(NSString*) textForPage:(NSUInteger)page hintView:(HintView*)hintView;
-(UIImage*) imageForPage:(NSUInteger)page hintView:(HintView*)hintView;
-(NSString*) titleForPage:(NSUInteger)page hintView:(HintView*)hintView;
-(UIImage*) titleIconForPage:(NSUInteger)page hintView:(HintView*)hintView;
-(NSString*)titleforHintView:(HintView*)hintView;


@end

@protocol HintViewDelegate<NSObject>
@optional
-(void) dismissedHintView:(HintView*)hintView;
-(CGFloat) heightForHintView:(HintView*)hintView;

@end
