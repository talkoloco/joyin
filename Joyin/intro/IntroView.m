#import "IntroView.h"
#import "TalkolocoUtils.h"

@implementation IntroView

- (id)initWithFrame:(CGRect)frame model:(IntroModel*)model
{
    self = [super initWithFrame:frame];
    if (self) {
        UILabel *titleLabel = [[UILabel alloc] init];
        [titleLabel setText:model.titleText];
        [titleLabel setTextColor:[UIColor blackColor]];
        [titleLabel setFont:[UIFont fontWithName:@"Zapfino" size:40]];
        //[titleLabel setShadowColor:[UIColor blackColor]];
        //[titleLabel setShadowOffset:CGSizeMake(1, 1)];
        //[titleLabel setBackgroundColor:[UIColor clearColor]];
        [titleLabel sizeToFit];
        [titleLabel setCenter:CGPointMake(frame.size.width/2, frame.size.height/2)];
        [self addSubview:titleLabel];
        
        UILabel *descriptionLabel = [[UILabel alloc] init];
        [descriptionLabel setText:model.descriptionText];
        [descriptionLabel setFont:[UIFont fontWithName:@"GillSans" size:20]];
        [descriptionLabel setTextColor:[UIColor blackColor]];
        [descriptionLabel setNumberOfLines:2];
        [descriptionLabel setBackgroundColor:[UIColor clearColor]];
        [descriptionLabel setTextAlignment:NSTextAlignmentCenter];
        [descriptionLabel sizeToFit];
        int w = descriptionLabel.frame.size.width * 0.75;
        int h = descriptionLabel.frame.size.height * 2;


        descriptionLabel.frame = CGRectMake(self.frame.size.width/2 - w/2, self.frame.size.height - 200, w, h);
        
        

        


        [self addSubview:descriptionLabel];
    
        
        UIImageView *imageView = [[UIImageView alloc] initWithImage:model.image];
        imageView.frame = CGRectMake(self.frame.size.width/2 - imageView.frame.size.width/6, self.frame.size.height/2 - imageView.frame.size.height/6 - titleLabel.frame.size.height + 20, imageView.frame.size.width/3, imageView.frame.size.height/3);
        
        [self addSubview: imageView];
        
    }
    return self;
}
@end
