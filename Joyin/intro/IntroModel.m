#import "IntroModel.h"

@implementation IntroModel

@synthesize titleText;
@synthesize descriptionText;
@synthesize image;

- (id) initWithTitle:(NSString*)title description:(NSString*)desc imageAsText:(NSString*)imageText {
    self = [super init];
    if(self != nil) {
        titleText = title;
        descriptionText = desc;
        image = [UIImage imageNamed:imageText];
    }
    return self;
}

- (id) initWithTitle:(NSString*)title description:(NSString*)desc image:(UIImage *)anyImage {
    self = [super init];
    if(self != nil) {
        titleText = title;
        descriptionText = desc;
        image = anyImage;
    }
    return self;
}

- (id) initWithTitle:(NSString*)title description:(NSString*)desc{
    self = [super init];
    if(self != nil) {
        titleText = title;
        descriptionText = desc;
    }
    return self;
}

@end
