#import "IntroControll.h"
#import "LoginViewController.h"
#import "TalkolocoUtils.h"

@implementation IntroControll


- (id)initWithFrame:(CGRect)frame pages:(NSArray*)pagesArray context:(UIViewController *)context
{
    self = [super initWithFrame:frame];
    if(self != nil) {
        
        //Initial Background images
        self.backgroundColor = [UIColor whiteColor];
        
        //Initial shadow
        UIView *shadowImageView = [UIView new];
        shadowImageView.frame = CGRectMake(0, 465, self.frame.size.width, 2);
        shadowImageView.alpha = 0.2;
        UIBezierPath *shadowPath = [UIBezierPath bezierPathWithRect:shadowImageView.bounds];
        shadowImageView.layer.masksToBounds = NO;
        shadowImageView.layer.shadowColor = [UIColor blackColor].CGColor;
        shadowImageView.layer.shadowOffset = CGSizeMake(0.0f, 5.0f);
        shadowImageView.layer.shadowOpacity = 0.5f;
        shadowImageView.layer.shadowPath = shadowPath.CGPath;
        shadowImageView.layer.masksToBounds = NO;
        [self addSubview:shadowImageView];
        
        //Initial ScrollView
        scrollView = [[UIScrollView alloc] initWithFrame:self.frame];
        scrollView.backgroundColor = [UIColor clearColor];
        scrollView.pagingEnabled = YES;
        scrollView.showsHorizontalScrollIndicator = NO;
        scrollView.showsVerticalScrollIndicator = NO;
        scrollView.delegate = self;
        [self addSubview:scrollView];
        
        UIView *whiteView = [UIView new];
        whiteView.frame  = CGRectMake(0, frame.size.height - 90, frame.size.width, 90);
        whiteView.backgroundColor = [UIColor whiteColor];

        
        UIButton *loginBtn = [UIButton buttonWithType:(UIButtonTypeRoundedRect)];
        
        double factor = 0.5;
        [loginBtn setFrame:CGRectMake(whiteView.frame.size.width/2 - 492*factor/2, whiteView.frame.size.height/2 - 20, 492*factor, 108*factor)];
        [loginBtn setBackgroundColor:[UIColor clearColor]];
        [loginBtn setBackgroundImage:[UIImage imageNamed:@"facebook-login-button.png"] forState:UIControlStateNormal];
        [loginBtn addTarget:context action:@selector(facebookLogin) forControlEvents:UIControlEventTouchUpInside];
        [whiteView addSubview:loginBtn];
        
        //Initial PageView
        pageControl = [[UIPageControl alloc] init];
        pageControl.frame = CGRectMake(whiteView.frame.size.width/2 -12, 0, 24, 24);
        pageControl.pageIndicatorTintColor = [UIColor lightGrayColor];
        pageControl.backgroundColor = [UIColor whiteColor];
        pageControl.currentPageIndicatorTintColor = [UIColor blueColor];
        pageControl.numberOfPages = pagesArray.count;
        
        [whiteView addSubview:pageControl];
        [self addSubview:whiteView];

        //Create pages
        pages = pagesArray;
        scrollView.contentSize = CGSizeMake(pages.count * frame.size.width, frame.size.height);
        
        currentPhotoNum = -1;
        
        //insert TextViews into ScrollView
        for(int i = 0; i <  pages.count; i++) {
            IntroView *view = [[IntroView alloc] initWithFrame:frame model:[pages objectAtIndex:i]];
            view.frame = CGRectOffset(view.frame, i*frame.size.width, 0);
            [scrollView addSubview:view];
        }
        [self bringSubviewToFront:shadowImageView];
        [self bringSubviewToFront:whiteView];


       [self initShow];
    }
    
    return self;
}

- (void) tick {
    [scrollView setContentOffset:CGPointMake((currentPhotoNum+1 == pages.count ? 0 : currentPhotoNum+1)*self.frame.size.width, 0) animated:YES];
}

- (void) initShow {
    NSUInteger scrollPhotoNumber = MAX(0, MIN(pages.count-1, (int)(scrollView.contentOffset.x / self.frame.size.width)));
    
    if(scrollPhotoNumber != currentPhotoNum) {
        currentPhotoNum = scrollPhotoNumber;
        
        //backgroundImage1.image = currentPhotoNum != 0 ? [(IntroModel*)[pages objectAtIndex:currentPhotoNum-1] image] : nil;
        //backgroundImage1.image = [(IntroModel*)[pages objectAtIndex:currentPhotoNum] image];
        //backgroundImage2.image = currentPhotoNum+1 != [pages count] ? [(IntroModel*)[pages objectAtIndex:currentPhotoNum+1] image] : nil;
    }
    
    float offset =  scrollView.contentOffset.x - (currentPhotoNum * self.frame.size.width);
    
    
    //left
    if(offset < 0) {
        pageControl.currentPage = 0;
        
        offset = self.frame.size.width - MIN(-offset, self.frame.size.width);
       // backgroundImage2.alpha = 0;
        //backgroundImage1.alpha = (offset / self.frame.size.width);
    
    //other
    } else if(offset != 0) {
        //last
        if(scrollPhotoNumber == pages.count-1) {
            pageControl.currentPage = pages.count-1;
            
           // backgroundImage1.alpha = 1.0 - (offset / self.frame.size.width);
        } else {
            
            pageControl.currentPage = (offset > self.frame.size.width/2) ? currentPhotoNum+1 : currentPhotoNum;
            
          //  backgroundImage2.alpha = offset / self.frame.size.width;
           // backgroundImage1.alpha = 1.0 - backgroundImage2.alpha;
        }
    //stable
    } else {
        pageControl.currentPage = currentPhotoNum;
      //  backgroundImage1.alpha = 1;
       // backgroundImage2.alpha = 0;
    }
}

- (void)scrollViewDidScroll:(UIScrollView *)scroll {
    [self initShow];
}

- (void) scrollViewDidEndDecelerating:(UIScrollView *)scroll {
    [self initShow];
}

@end
