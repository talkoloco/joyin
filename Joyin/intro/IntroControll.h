#import <UIKit/UIKit.h>
#import "IntroView.h"

@interface IntroControll : UIView<UIScrollViewDelegate> {
    UIImageView *backgroundImage1;
    UIImageView *backgroundImage2;
    
    UIScrollView *scrollView;
    UIPageControl *pageControl;
    NSArray *pages;
    NSUInteger currentPhotoNum;
}

- (id)initWithFrame:(CGRect)frame pages:(NSArray*)pages context:(UIViewController *)context;



@end
