//
//  TalkolocoUtils.m
//  Talkoloco
//
//  Created by Ohad on 2013-07-27.
//  Copyright (c) 2013 Talkoloco. All rights reserved.
//

#import "TalkolocoUtils.h"
#import "Group.h"
#import "ChatViewController.h"
#import "AppDelegate.h"
#import "TalkolocoUser.h"
//#import "Flurry.h"
#import "NSString+JRStringAdditions.h"
#import "LoginViewController.h"
#import "SBJson.h"
#import "BDKNotifyHUD.h"
#import "DejalActivityView.h"


@implementation TalkolocoUtils

+ (TalkolocoUtils *)sharedInstance
{
    static TalkolocoUtils *sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[TalkolocoUtils alloc] init];
        sharedInstance.shouldCheckLocation = YES;
        //sharedInstance.shouldContinueBeta = YES;
        sharedInstance.serverIsDown = NO;
        //sharedInstance.shouldForceUpgrade = NO;
    });
    return sharedInstance;
}

-(TalkolocoUser *)parseResponseToUser:(NSDictionary *)item{
    TalkolocoUser *user = [TalkolocoUser new];
    user.userId = [item objectForKey:@"id"];
    user.privateGroups = [item objectForKey:@"privateGroups"];
    user.userName = [item objectForKey:@"username"];
    user.deviceToken = [item objectForKey:@"deviceToken"];
    NSLog(@"%@", [item description]);
    NSDictionary *facebookDetails = [item objectForKey:@"facebookDetails"];
    if (facebookDetails!=nil){
        user.facebookID = [facebookDetails objectForKey:@"facebook_id"];
    }
    return user;

}

-(NSMutableURLRequest *)buildGetRequest:(NSString *)urlStr{
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlStr]];
    [request setHTTPMethod:@"GET"];
    [request setValue:@"application/x-www-form-urlencoded charset=utf-8" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPShouldHandleCookies:YES];
    return request;
}

-(NSMutableURLRequest *)buildPostRequest:(NSString *)urlStr{
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlStr]];
    [request setHTTPMethod:@"POST"];
    [request setValue:@"application/x-www-form-urlencoded charset=utf-8" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPShouldHandleCookies:YES];
    return request;
}

-(NSMutableURLRequest *)buildPutRequest:(NSString *)urlStr{
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlStr]];
    [request setHTTPMethod:@"PUT"];
    [request setValue:@"application/x-www-form-urlencoded charset=utf-8" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPShouldHandleCookies:YES];
    return request;
}

- (NSMutableURLRequest *)buildRequest:(NSString *)urlStr withPostData:(NSData *)postData{
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlStr]];
    [request setHTTPMethod:@"POST"];
    [request setValue:[NSString stringWithFormat:@"%lu", (unsigned long)postData.length] forHTTPHeaderField:@"Content-Length"];
    [request setValue:@"application/x-www-form-urlencoded charset=utf-8" forHTTPHeaderField:@"Content-Type"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:postData];
    [request setHTTPShouldHandleCookies:YES];
    return request;
}

- (NSMutableArray *)parseResponseToGroups:(NSArray *)jsonArray{
    NSMutableArray *mGroups = [NSMutableArray new];
    for (NSDictionary *item in jsonArray) {
        Group *group  = [self parseResponseToGroup:item];
        [mGroups addObject:group];
    }
    return mGroups;
}

- (Group *)parseResponseToGroup:(NSDictionary *)item{
        Group *group = [Group new];
        group.groupId = [item objectForKey:@"id"];
        group.isPrivate = [[item objectForKey:@"isPrivate"] boolValue];
        group.longitude = [(NSString *)[item objectForKey:@"longitude"] doubleValue];
        group.latitude = [(NSString *)[item objectForKey:@"latitude"] doubleValue];
        group.creatorId = [item objectForKey:@"creatorId"];
        group.placePicURL = [item objectForKey:@"placePicURL"];
        group.placeName = [item objectForKey:@"placeName"];
        group.message = [item objectForKey:@"message"];
        group.groupName = [item objectForKey:@"groupName"];
        group.address = [item objectForKey:@"address"];
        NSArray *members = (NSArray *)[item objectForKey:@"members"];
        group.members = members;
        group.size = [members count];
        NSString *dateString = [item objectForKey:@"created"];
        double timeIntervalInSecs = [dateString doubleValue]/1000;
        group.created = [NSDate dateWithTimeIntervalSince1970: timeIntervalInSecs];
        group.lastMessage =[item objectForKey:@"lastMessage"];
    
    
        NSString *lastSeenSomeoneAtPlace = [item objectForKey:@"lastSeenSomeoneAtPlace"];
        timeIntervalInSecs = [lastSeenSomeoneAtPlace doubleValue]/1000;
        NSDate *lastSeenSomeoneAtPlaceDate = [NSDate dateWithTimeIntervalSince1970: timeIntervalInSecs];
        NSDate *now = [NSDate date];
    
        NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
        NSDateComponents *components = [calendar components:NSSecondCalendarUnit
                                               fromDate:lastSeenSomeoneAtPlaceDate
                                                 toDate:now
                                                options:0];
        int awayTimeInSeconds = (int)components.second;
    
        NSString *someoneIsAtPlace = [[item objectForKey:@"someoneIsAtPlace"] stringValue];
        BOOL IsSomeoneIsAtPlace = [someoneIsAtPlace isEqualToString:@"1"]? YES:NO;
        IsSomeoneIsAtPlace = IsSomeoneIsAtPlace && (awayTimeInSeconds < MAX_AWAY_TIME_IN_SEC);
    
        group.someoneAtPlace = IsSomeoneIsAtPlace;
        return group;
}

- (void)locationManager:(CLLocationManager *)manager
	 didUpdateLocations:(NSArray *)locations{
    CLLocation *location = [locations objectAtIndex:0];
    if (_shouldCheckLocation){
        _shouldCheckLocation = NO;
        _userLocation = location;
    }
   // NSLog(@"latitude: %f, longitude: %f",_userLocation.coordinate.latitude,_userLocation.coordinate.longitude);
}

-(void)saveUserLocation:(CLLocation *)location{
    NSString *userId = [[NSUserDefaults standardUserDefaults] stringForKey:@"userId"];

    if (userId ==nil || location==nil || location.coordinate.longitude==0 || location.coordinate.latitude==0)
        return;
    NSString *post = [NSString stringWithFormat:@"{\"longitude\":\"%f\",\"latitude\":\"%f\",\"userId\":\"%@\"}",location.coordinate.longitude, location.coordinate.latitude, userId];
    //NSLog(@"%@",post);
    NSData *postData = [post dataUsingEncoding:NSUTF8StringEncoding];
    NSString *urlStr = [NSString stringWithFormat:@"http://%@:%@/saveUserLocation",IPADDRESS, PORT];
    NSMutableURLRequest *request = [self buildRequest:urlStr withPostData:postData];
    NSOperationQueue *queue = [[NSOperationQueue alloc] init];
    [NSURLConnection sendAsynchronousRequest:request queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
     {
         if ([data length] == 0 || error != nil){
             NSLog(@"Error: %@", [error description]);
         }
     }];

}

-(void)startLocationServices{
    self.locationManager = [[CLLocationManager alloc] init];
    self.locationManager.delegate = self;
    self.locationManager.desiredAccuracy = kCLLocationAccuracyBestForNavigation;
    [self.locationManager startUpdatingLocation];
    
    
}

-(void)stopLocationServices{
    [self.locationManager stopUpdatingLocation];
}

-(void)showBetaClosedMessage{
    UIAlertView *message = [[UIAlertView alloc] initWithTitle:@"The Beta is closed"
                                                      message:@"Check talkoloco.com for more details"
                                                     delegate:self
                                            cancelButtonTitle:nil
                                            otherButtonTitles:nil];
    [message show];
    [self performSelector:@selector(openSite:) withObject:message afterDelay:3];
}

-(void)openSite:(UIAlertView *)message{
    NSLog(@"URL - %@",self.appUrl);
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:self.appUrl]];
    [message dismissWithClickedButtonIndex:0 animated:YES];
}

-(void)showForceUpgradeMessage{
    UIAlertView *message = [[UIAlertView alloc] initWithTitle:@"New version is available Please upgrade"
                                                      message:nil
                                                     delegate:self
                                            cancelButtonTitle:nil
                                            otherButtonTitles:nil];
    [message show];
    [self performSelector:@selector(openSite:) withObject:message afterDelay:3];
}

-(void)removeUserFromGroup:(NSString *)groupId withContext:(UIViewController *)controller{
    NSString *userId = [[NSUserDefaults standardUserDefaults]
                        stringForKey:@"userId"];
    NSString *post = [NSString stringWithFormat:@"{\"groupId\":\"%@\",\"userId\":\"%@\"}",groupId, userId];
    //NSLog(@"%@",post);
    NSData *postData = [post dataUsingEncoding:NSUTF8StringEncoding];
    NSString *urlStr = [NSString stringWithFormat:@"http://%@:%@/removeUserFromGroup",IPADDRESS, PORT];
    NSMutableURLRequest *request = [self buildRequest:urlStr withPostData:postData];
    NSOperationQueue *queue = [[NSOperationQueue alloc] init];
    [NSURLConnection sendAsynchronousRequest:request queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
     {
         if ([data length] > 0 && error == nil){
             NSString *str=[[NSString alloc]initWithData:data encoding:NSUTF8StringEncoding];
             NSLog(str, @"%@");
         }
         else {
             NSLog(@"Error: %@", [error description]);
         }
         [controller performSelectorOnMainThread:@selector(stopLoading) withObject:nil waitUntilDone:NO];
         
     }];
}



-(BOOL)isGuest{
    NSString *fbAccessToken = [[NSUserDefaults standardUserDefaults] stringForKey:@"fbAccessToken"];
    return fbAccessToken==nil;
}

-(void)facebookLogin :(id<FacebookLoginDelegate>)delegate{
    [FBSession openActiveSessionWithReadPermissions:@[@"basic_info", @"email"]
                                       allowLoginUI:YES
                                  completionHandler:^(FBSession *session,
                                                      FBSessionState status,
                                                      NSError *error) {
                                      
                                      NSString *fbAccessToken = [[FBSession.activeSession accessTokenData] accessToken];
                                      
                                      //NSDate *fbExpirationDate = [[FBSession.activeSession accessTokenData] expirationDate];
                                      
                                      [[NSUserDefaults standardUserDefaults] setObject:fbAccessToken forKey:@"fbAccessToken"];
                                      
                                      //NSString *deviceToken = [[NSUserDefaults standardUserDefaults] objectForKey:@"deviceToken"];
                                      
                                      [self facebookTalkoLogin];
                                      [delegate onLoginComplete];
                                  }];
}

-(void)facebookLogin{
    [self facebookLogin:nil];
}

-(void)facebookTalkoLogin{
    [self facebookTalkoLogin: nil];
}

-(void)facebookTalkoLogin:(void (^)(void))handler{

    NSString *fbToken  = [[NSUserDefaults standardUserDefaults] stringForKey:@"fbAccessToken"];
    NSString *deviceToken = [[NSUserDefaults standardUserDefaults] objectForKey:@"deviceToken"];

    NSString *post = [NSString stringWithFormat:@"{\"token\":\"%@\", \"deviceToken\":\"%@\", \"deviceType\":\"%@\"}", fbToken, deviceToken, @"iPhone"];
    NSLog(@"%@",post);
    NSData *postData = [post dataUsingEncoding:NSUTF8StringEncoding];
    
    NSString *urlStr = [NSString stringWithFormat:@"http://%@:%@/login/facebookWithDevice",IPADDRESS, PORT];
    NSMutableURLRequest *request = [[TalkolocoUtils sharedInstance] buildRequest:urlStr withPostData:postData];
    
    NSOperationQueue *queue = [[NSOperationQueue alloc] init];
    [NSURLConnection sendAsynchronousRequest:request queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
     {
         if ([data length] > 0 && error == nil) {
             [self receiveLoggedInUser:data withHandler:handler];
         } else {
             NSLog(@"Error: %@", [error description]);
         }

         
     }];
}

-(void)receiveLoggedInUser:(NSData *)data withHandler :(void (^)(void))handler {
    NSString *strUser=[[NSString alloc]initWithData:data encoding:NSUTF8StringEncoding];
    NSLog(@"User - %@", strUser);
    NSDictionary *dUser=[NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
    TalkolocoUser *tUser= [[TalkolocoUtils sharedInstance] parseResponseToUser:dUser];
    [[TalkolocoUtils sharedInstance] updateUserDefaultsWithUser:tUser];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        if (handler) {
            handler();
        }
    });
}


-(BOOL)isPhone5{
    CGSize result = [[UIScreen mainScreen] bounds].size;
    if (result.height >= 500){
        return YES;
    }else{
        return NO;
    }
}


-(BOOL)receiveData:(NSData *)data{
    NSString *responde=[[NSString alloc]initWithData:data encoding:NSUTF8StringEncoding];
    NSLog(@"OK? - %@", responde);
    if ([responde isEqualToString:@"Ok"]){
        return YES;
    }
    return NO;
}

-(void)parseCookieFromRespose:(NSURLResponse *)response{
    NSString *userCookie = nil;
    NSString *playSession = nil;
    NSString *facebookId = nil;
    
    NSHTTPURLResponse *HTTPResponse = (NSHTTPURLResponse *)response;
    NSDictionary *fields = [HTTPResponse allHeaderFields];
    NSString *cookie = [fields valueForKey:@"Set-Cookie"]; //
    NSLog(@"%@", cookie);
    if (cookie!=nil){
        NSArray *stringArray = [cookie componentsSeparatedByString: @"PLAY_SESSION="];
        stringArray = [(NSString *)stringArray[1] componentsSeparatedByString:@"-username"];
        playSession = stringArray[0];
        
        NSString *restOfString = stringArray[1];
        if ([restOfString containsString:@"facebookId"]){
            stringArray = [restOfString componentsSeparatedByString:@"%00facebookId%"];
            userCookie  = stringArray[0];
            stringArray = [(NSString *)stringArray[0] componentsSeparatedByString:@"; Path="];
            facebookId = stringArray[0];
        } else {
            stringArray = [(NSString *)stringArray[0] componentsSeparatedByString:@"; Path="];
            userCookie = stringArray[0];
        }
        
        [[NSUserDefaults standardUserDefaults] setObject:userCookie forKey:@"userCookie"];
        [[NSUserDefaults standardUserDefaults] setObject:playSession forKey:@"playSession"];
        
        if (facebookId!=nil){
            [[NSUserDefaults standardUserDefaults] setObject:facebookId forKey:@"facebookId"];

        }
    }
}

-(void)addCurrentUserToGroup:(NSString *)groupId{
    NSString *userId = [[NSUserDefaults standardUserDefaults] objectForKey:@"userId"];
    NSString *urlStr = [NSString stringWithFormat:@"http://%@:%@/group/%@/add/user/%@",IPADDRESS, PORT,groupId,userId];
    NSMutableURLRequest *request = [[TalkolocoUtils sharedInstance] buildPutRequest:urlStr];
    NSOperationQueue *queue = [[NSOperationQueue alloc] init];
    [NSURLConnection sendAsynchronousRequest:request queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
     {
         if ([data length] > 0 && error == nil) {
             NSString *str=[[NSString alloc]initWithData:data encoding:NSUTF8StringEncoding];
             NSLog(@"%@",str);
         }
     }];
}

-(void)inviteWithUserId:(NSString *)userId toGroup:(NSString *)groupId onView:(UIView *)view withActivityView:(BOOL)withActivityView withCompletionHandler:(void (^)(void))handler{
    if (withActivityView){
        [DejalBezelActivityView activityViewForView:view withLabel:@"Request to join..."];
    }
    
    NSString *urlStr = [NSString stringWithFormat:@"http://%@:%@/group/%@/add/user/%@",IPADDRESS, PORT,groupId,userId];
    NSMutableURLRequest *request = [[TalkolocoUtils sharedInstance] buildPutRequest:urlStr];
    NSOperationQueue *queue = [[NSOperationQueue alloc] init];
    [NSURLConnection sendAsynchronousRequest:request queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
     {
         [self inviteWithUserIdData:data withError:error onView:view withGroupId:groupId withCompletionHandler:handler];
     }];
}

-(void)inviteWithUserId:(NSString *)userId toGroup:(NSString *)groupId onView:(UIView *)view withCompletionHandler:(void (^)(void))handler{
    [self inviteWithUserId:userId toGroup:groupId onView:view withActivityView:NO withCompletionHandler:handler];
         
}

-(void)inviteWithUserIdData:(NSData *)data withError:(NSError *)error onView:(UIView *)view withGroupId: (NSString *)groupId withCompletionHandler:(void (^)(void))handler{
    if ([data length] > 0 && error == nil) {
        NSString *str=[[NSString alloc]initWithData:data encoding:NSUTF8StringEncoding];
        NSString *text = nil;
        Group *group = nil;
        BOOL shouldDispatchAlert = YES;
        //[self performSelectorOnMainThread:@selector(alertString:) withObject:@"0" waitUntilDone:NO];
        if ([str containsString:@"Group was not found"]){
            text = @"Group was not found";
        } else if ([str containsString:@"already member"]){
            text = str;
        } else if ([str containsString:@"Authentication Error"]){
            shouldDispatchAlert = NO;
            //[self performSelectorOnMainThread:@selector(alertString:) withObject:@"1" waitUntilDone:NO];
            [[TalkolocoUtils sharedInstance] facebookTalkoLogin:^{
               // [self performSelectorOnMainThread:@selector(alertString:) withObject:@"2" waitUntilDone:NO];
                NSString *userId = [[NSUserDefaults standardUserDefaults] objectForKey:@"userId"];
                [self inviteWithUserId:userId toGroup:groupId onView:view withActivityView:NO withCompletionHandler:handler];
            }];
        } else {
            NSDictionary *groupDic = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
            group = [[TalkolocoUtils sharedInstance] parseResponseToGroup:groupDic];
            text = @"You've joined";
        }
        if (shouldDispatchAlert){
            dispatch_async(dispatch_get_main_queue(), ^{
                [self alertUserInvitedToGroup:group onView:view withText:text withCompletionHandler:handler];
            });
        }
        
        NSLog(@"response - %@", str);
    } else {
        NSLog(@"Error: %@", [error description]);
    }

}

-(void)alertString:(NSString *)string{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Talkoloco"
                                                    message:string
                                                   delegate:nil
                                          cancelButtonTitle:@"OK"
                                          otherButtonTitles:nil];
    [alert show];
}

-(void)alertUserInvitedToGroup:(Group *)group onView: (UIView *)view withText:(NSString *)text withCompletionHandler:(void (^)(void))handler{
    [DejalBezelActivityView removeViewAnimated:YES];

    NSString *groupName = nil;
    NSString *imgString = nil;
    
    if (group==nil){
        imgString = @"checkmark_white";//@"white-question";
        groupName  = @"";
    } else {
        groupName = group.groupName;
        imgString = @"checkmark_white";
    }
    
    BDKNotifyHUD *hud = [BDKNotifyHUD notifyHUDWithImage:[UIImage imageNamed:imgString] text:[NSString stringWithFormat:@"%@ %@", text, groupName]];
    
    hud.center = CGPointMake(view.center.x, view.center.y - 20);
    
    // Animate it, then get rid of it. These settings last 1 second, takes a half-second fade.
    [view addSubview:hud];
    [hud presentWithDuration:2.0f speed:1.0f inView:view completion:^{
        [hud removeFromSuperview];
        dispatch_async(dispatch_get_main_queue(), ^{
            if (handler) {
                handler();
            }
        });
    }];
}

-(void)alertUserAleadyMember{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Talkoloco"
                                                    message:@"User is aleady a member of group"
                                                   delegate:self
                                          cancelButtonTitle:@"OK"
                                          otherButtonTitles:nil];
    [alert show];
}

-(void)alertGroupWasNotFound{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Talkoloco"
                                                    message:@"Group was not found, please check your input param - groupId"
                                                   delegate:self
                                          cancelButtonTitle:@"OK"
                                          otherButtonTitles:nil];
    [alert show];
}


-(void)updateUserDefaultsWithUser:(TalkolocoUser *)tUser{
    if (tUser.userName!=nil)
        [[NSUserDefaults standardUserDefaults] setObject:tUser.userName forKey:@"username"];
    if (tUser.userId!=nil)
        [[NSUserDefaults standardUserDefaults] setObject:tUser.userId forKey:@"userId"];
    if (tUser.facebookID!=nil)
        [[NSUserDefaults standardUserDefaults] setObject:tUser.facebookID forKey:@"facebookId"];
    if (tUser.privateGroups!=nil)
        self.userPrivateGroups = tUser.privateGroups;
    [[NSUserDefaults standardUserDefaults] synchronize];
    
}





@end
