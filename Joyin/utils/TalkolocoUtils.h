//
//  TalkolocoUtils.h
//  Talkoloco
//
//  Created by Ohad on 2013-07-27.
//  Copyright (c) 2013 Talkoloco. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>
#import "Group.h"
#import "TalkolocoUser.h"
//#import "Notification.h"
#import "LoginViewController.h"

//#define IPADDRESS @"10.100.102.7"
#define IPADDRESS @"192.99.11.67"
//#define PORT @"9000"
#define PORT @"80"
#define FONT @"Thonburi"
#define S_FONT @"ChalkboardSE-Regular"
#define FONT_BOLD @"Thonburi-Bold"
#define kViewRoundedCornerRadius 10.0f
#define RADIUS @"1.3164354E-4"
#define FACTOR 1000000000
#define MAX_AWAY_TIME_IN_SEC 60*15
#define PLACE_NAME @"Spot name:"
#define MAX_GROUPS_PER_PLACE 5

@interface TalkolocoUtils : NSObject <CLLocationManagerDelegate>
// user location
@property (nonatomic, strong) CLLocationManager *locationManager;
@property (nonatomic, strong) CLLocation *userLocation;
@property (nonatomic, strong) Group *userCurrentGroup;
@property (nonatomic, strong) NSArray *userPrivateGroups;
@property (nonatomic, readwrite) BOOL shouldCheckLocation;
@property (nonatomic, strong) NSString *appUrl;
@property (nonatomic, readwrite) BOOL serverIsDown;

+ (TalkolocoUtils *)sharedInstance;
-(void)startLocationServices;
-(void)stopLocationServices;
- (NSMutableURLRequest *)buildRequest:(NSString *)urlStr withPostData:(NSData *)postData;
- (NSMutableURLRequest *)buildGetRequest:(NSString *)urlStr;
- (NSMutableURLRequest *)buildPostRequest:(NSString *)urlStr;
-(NSMutableURLRequest *)buildPutRequest:(NSString *)urlStr;
- (NSMutableArray *)parseResponseToGroups:(NSArray *)jsonArray;
- (Group *)parseResponseToGroup:(NSDictionary *)item;
//- (Notification *)parseResponseToNotification:(NSDictionary *)item;
- (NSMutableArray *)parseResponseToNotifications:(NSArray *)jsonArray;



-(TalkolocoUser *)parseResponseToUser:(NSDictionary *)item;
//-(void)checkBetaStatus:(void (^)(void))handler;
-(void)showBetaClosedMessage;
-(void)showForceUpgradeMessage;
-(void)saveUserLocation:(CLLocation *)location;
-(void)removeUserFromGroup:(NSString *)groupId withContext:(UIViewController *)controller;
-(BOOL)isPhone5;
-(BOOL)isGuest;
-(void)facebookLogin :(id<FacebookLoginDelegate>)delegate;
-(void)facebookLogin;
-(void)facebookTalkoLogin;
-(void)inviteWithUserId:(NSString *)userId toGroup:(NSString *)groupId onView:(UIView *)view withCompletionHandler:(void (^)(void))handler;
-(void)inviteWithUserId:(NSString *)userId toGroup:(NSString *)groupId onView:(UIView *)view withActivityView:(BOOL)withActivityView withCompletionHandler:(void (^)(void))handler;
-(void)facebookTalkoLogin:(void (^)(void))handler;
-(void)updateUserDefaultsWithUser:(TalkolocoUser *)tUser;
-(void)parseCookieFromRespose:(NSURLResponse *)response;
-(void)addCurrentUserToGroup:(NSString *)groupId;

@end
