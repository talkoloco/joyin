#import "BubbleView.h"
#import "MessageInputView.h"
#import "NSString+MessagesView.h"
#import "TalkolocoUtils.h"

#define kMarginTop 8.0f
#define kMarginBottom 4.0f
#define kPaddingTop 4.0f
#define kPaddingBottom 8.0f
#define kUserNameHeight 18.0f
#define kBubblePaddingRight 35.0f
#define kTimeWidth 20.0f


@interface BubbleView()

- (void)setup;
- (BOOL)styleIsOutgoing;

@end



@implementation BubbleView

@synthesize style;
@synthesize text;
@synthesize userName;
@synthesize userColor;
@synthesize isAtPlace;
@synthesize isReceived;
@synthesize time;


#pragma mark - Initialization
- (void)setup
{
    self.backgroundColor = [UIColor clearColor];
}

- (id)initWithFrame:(CGRect)frame bubbleStyle:(BubbleMessageStyle)bubbleStyle
{
    self = [super initWithFrame:frame];
    if(self) {
        [self setup];
        self.style = bubbleStyle;
    }
    return self;
}

#pragma mark - Setters
- (void)setStyle:(BubbleMessageStyle)newStyle
{
    style = newStyle;
    [self setNeedsDisplay];
}

- (void)setText:(NSString *)newText
{
    text = newText;
    [self setNeedsDisplay];
}

#pragma mark - Drawing
- (void)drawRect:(CGRect)frame
{
	UIImage *image = [BubbleView bubbleImageForStyle:self.style];
    CGSize bubbleSize = [BubbleView bubbleSizeForText:self.text withUserName: self.userName];
	CGRect bubbleFrame = CGRectMake(([self styleIsOutgoing] ? self.frame.size.width - bubbleSize.width : 0.0f),
                                    kMarginTop,
                                    bubbleSize.width,
                                    bubbleSize.height);
    
	[image drawInRect:bubbleFrame];
	
	CGSize textSize = [BubbleView textSizeForText:self.text];
    CGSize userNameSize = [BubbleView textSizeForText:self.userName];

	CGFloat textX = (CGFloat)image.leftCapWidth - 3.0f + ([self styleIsOutgoing] ? bubbleFrame.origin.x : 0.0f);
    CGRect textFrame = CGRectMake(textX,
                                  kPaddingTop + kMarginTop + userNameSize.height - 2,
                                  textSize.width,
                                  textSize.height);
    
    
    NSDictionary *atts = [[NSDictionary alloc] initWithObjectsAndKeys:[BubbleView font],NSFontAttributeName, nil];
    [self.text drawInRect:textFrame withAttributes:atts];
    
    CGRect userNameFrame = CGRectMake(textX,
                                      kPaddingTop + kMarginTop,
                                      userNameSize.width,
                                      userNameSize.height);
    
    UIColor *textColor = nil;
    
    if (self.style==BubbleMessageStyleOutgoingDefault)
        textColor = [UIColor blueColor];
    else {
        textColor = self.userColor;
    }
    
    NSDictionary *attsUserName = [[NSDictionary alloc] initWithObjectsAndKeys:textColor,NSForegroundColorAttributeName, nil];
    [self.userName drawInRect:userNameFrame withAttributes:attsUserName];
    
    int pWidth = MAX(userNameFrame.size.width, textFrame.size.width) + userNameFrame.origin.x;
    
    CGRect pFrame = CGRectMake(pWidth + 2, userNameFrame.origin.y, 15,15);
    
    if (self.isAtPlace){
        //[@"@" drawInRect:pFrame withAttributes:attsUserName];
        UIImage *location = [UIImage imageNamed:@"aplace.png"];
        [location drawInRect:pFrame];
        //[@"@" drawInRect:pFrame withAttributes:nil];
    }
    
    CGRect tFrame = CGRectMake(pWidth + 15, userNameFrame.origin.y, 15,15);
    
    //if (self.style==BubbleMessageStyleOutgoingDefault){

    
        if (self.isReceived){
            UIImage *clock = [UIImage imageNamed:@"tick3.png"];
            [clock drawInRect:tFrame];
        } else {
            UIImage *tick = [UIImage imageNamed:@"clock_blue.png"];
            [tick drawInRect:tFrame];
        }
        
    //}
    
    CGRect timeFrame;
    if (self.style==BubbleMessageStyleIncomingDefault)
        timeFrame = CGRectMake(tFrame.origin.x -10 , bubbleFrame.origin.y + bubbleFrame.size.height - 22, 45, 15);
    else
        timeFrame = CGRectMake(tFrame.origin.x -10 , bubbleFrame.origin.y + bubbleFrame.size.height - 22, 45, 15);
    
    
    NSDictionary *attsTime = @{NSFontAttributeName : [UIFont systemFontOfSize:10.0f]};
    
    //NSLog(@"time %@",self.time);
    if (self.time!=nil)
        [self.time drawInRect:timeFrame withAttributes:attsTime];
}

#pragma mark - Bubble view
- (BOOL)styleIsOutgoing
{
    return (self.style == BubbleMessageStyleOutgoingDefault
            || self.style == BubbleMessageStyleOutgoingDefaultGreen
            || self.style == BubbleMessageStyleOutgoingSquare);
}

+ (UIImage *)bubbleImageForStyle:(BubbleMessageStyle)style
{
    switch (style) {
        case BubbleMessageStyleIncomingDefault:
            return [[UIImage imageNamed:@"bubbleSquareIncoming"] stretchableImageWithLeftCapWidth:23 topCapHeight:15];
        case BubbleMessageStyleIncomingSquare:
            return [[UIImage imageNamed:@"bubbleSquareIncoming"] stretchableImageWithLeftCapWidth:25 topCapHeight:15];
            break;
            break;
        case BubbleMessageStyleOutgoingDefault:
            return [[UIImage imageNamed:@"rightBubleBig"] stretchableImageWithLeftCapWidth:15 topCapHeight:15];
            break;
        case BubbleMessageStyleOutgoingDefaultGreen:
            return [[UIImage imageNamed:@"messageBubbleGreen"] stretchableImageWithLeftCapWidth:15 topCapHeight:15];
            break;
        case BubbleMessageStyleOutgoingSquare:
            return [[UIImage imageNamed:@"bubbleSquareOutgoing"] stretchableImageWithLeftCapWidth:15 topCapHeight:15];
            break;
    }
    
    return nil;
}

+ (UIFont *)font
{
    return [UIFont systemFontOfSize:16.0f];
}

+ (CGSize)textSizeForText:(NSString *)txt
{
    CGFloat width = [UIScreen mainScreen].applicationFrame.size.width * 0.65f;
    CGFloat height = MAX([BubbleView numberOfLinesForMessage:txt],
                         [txt numberOfLines]) * [MessageInputView textViewLineHeight];
    NSDictionary *atts = [[NSDictionary alloc] initWithObjectsAndKeys:[BubbleView font],NSFontAttributeName, nil];
    
    return [txt boundingRectWithSize:CGSizeMake(width, height) options:NSStringDrawingUsesLineFragmentOrigin attributes:atts context:nil].size;
    
    
}

+ (CGSize)bubbleSizeForText:(NSString *)txt withUserName:(NSString *)userName
{
    
    NSDictionary *atts = [[NSDictionary alloc] initWithObjectsAndKeys:[BubbleView font],NSFontAttributeName, nil];

    float textWidth = [txt sizeWithAttributes:atts].width;
    float nameWidth = [userName sizeWithAttributes: atts].width;
    //float textWidth = [txt sizeWithFont:[BubbleView font]].width;
    //float nameWidth = [userName sizeWithFont:[BubbleView font]].width;
    NSString *biggerStr = textWidth > nameWidth ? txt :userName;
    
	CGSize textSize = [BubbleView textSizeForText:biggerStr];
	return CGSizeMake(textSize.width + kBubblePaddingRight +kTimeWidth,
                      textSize.height + kPaddingTop + kPaddingBottom + kUserNameHeight);
}

+ (CGFloat)cellHeightForText:(NSString *)txt withUserName:(NSString *)userName
{
    return [BubbleView bubbleSizeForText:txt withUserName:userName].height + kMarginTop + kMarginBottom + 10;
}

+ (int)maxCharactersPerLine
{
    return ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPhone) ? 33 : 109;
}

+ (int)numberOfLinesForMessage:(NSString *)txt
{
    return (txt.length / [BubbleView maxCharactersPerLine]) + 1.0;
}

@end