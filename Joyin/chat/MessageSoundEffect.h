#import <Foundation/Foundation.h>
#import <AudioToolbox/AudioToolbox.h>

@interface MessageSoundEffect : NSObject

+ (void)playMessageReceivedSound;
+ (void)playMessageSentSound;

@end