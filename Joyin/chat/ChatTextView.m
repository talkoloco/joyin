//
//  ChatTextView.m
//  Talkoloco
//
//  Created by Ohad on 2014-04-08.
//  Copyright (c) 2014 Talkoloco. All rights reserved.
//

#import "ChatTextView.h"

@implementation ChatTextView

- (CGRect)caretRectForPosition:(UITextPosition *)position {
    CGRect originalRect = [super caretRectForPosition:position];
    originalRect.size.height = 18;
    return originalRect;
}

@end
