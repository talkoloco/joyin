
#import <UIKit/UIKit.h>
#import "BubbleMessageCell.h"
#import "BubbleView.h"
#import "MessageInputView.h"
#import "MessagesoundEffect.h"
#import "UIButton+MessagesView.h"

typedef enum {
    MessagesViewTimestampPolicyAll = 0,
    MessagesViewTimestampPolicyAlternating,
    MessagesViewTimestampPolicyEveryThree,
    MessagesViewTimestampPolicyEveryFive,
    MessagesViewTimestampPolicyCustom
} MessagesViewTimestampPolicy;



@protocol MessagesViewDelegate <NSObject>
@required
- (void)sendPressed:(UIButton *)sender withText:(NSString *)text;
- (BubbleMessageStyle)messageStyleForRowAtIndexPath:(NSIndexPath *)indexPath;
- (MessagesViewTimestampPolicy)timestampPolicyForMessagesView;
- (BOOL)hasTimestampForRowAtIndexPath:(NSIndexPath *)indexPath;
@end



@protocol MessagesViewDataSource <NSObject>
@required
- (NSString *)textForRowAtIndexPath:(NSIndexPath *)indexPath;
- (NSDate *)timestampForRowAtIndexPath:(NSIndexPath *)indexPath;
- (NSString *)userNameForRowAtIndexPath:(NSIndexPath *)indexPath;
- (NSString *)senderIdForRowAtIndexPath:(NSIndexPath *)indexPath;
- (BOOL)userAtPlaceAtIndexPath:(NSIndexPath *)indexPath;
- (BOOL)messageReceivedAtPlaceAtIndexPath:(NSIndexPath *)indexPath;

@end



@interface TalksViewController : UIViewController <UITableViewDataSource, UITableViewDelegate, UITextViewDelegate>{
    NSArray *colors;
    NSMutableDictionary *colorPerUser;
}

@property (weak, nonatomic) id<MessagesViewDelegate> delegate;
@property (weak, nonatomic) id<MessagesViewDataSource> dataSource;
@property (strong, nonatomic) UITableView *tableView;
@property (strong, nonatomic) MessageInputView *inputView;
@property (assign, nonatomic) CGFloat previousTextViewContentHeight;
@property (assign, nonatomic) BOOL chatIsReadOnly;
@property (assign, nonatomic) BOOL cancelKeyboardNotifications;


#pragma mark - Initialization
- (UIButton *)sendButton;

#pragma mark - Actions
- (void)sendPressed:(UIButton *)sender;
- (void)handleSwipe:(UIGestureRecognizer *)guestureRecognizer;

#pragma mark - Messages view controller
- (BOOL)shouldHaveTimestampForRowAtIndexPath:(NSIndexPath *)indexPath;
- (void)finishSend;
- (void)setBackgroundColor:(UIColor *)color;
- (void)scrollToBottomAnimated:(BOOL)animated;

#pragma mark - Keyboard notifications
- (void)handleWillShowKeyboard:(NSNotification *)notification;
- (void)handleWillHideKeyboard:(NSNotification *)notification;
- (void)keyboardWillShowHide:(NSNotification *)notification;

@end