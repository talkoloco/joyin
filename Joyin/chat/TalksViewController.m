#import "TalksViewController.h"
#import "NSString+MessagesView.h"
#import "UIView+AnimationOptionsForCurve.h"
#import "UIColor+MessagesView.h"

#define INPUT_HEIGHT 40.0f
//#define NAV_BAR_HEIGHT 49.0f
#define NAV_BAR_HEIGHT 0


@interface TalksViewController ()

- (void)setup;

@end



@implementation TalksViewController

#pragma mark - Initialization
- (void)setup
{
    if([self.view isKindOfClass:[UIScrollView class]]) {
        // fix for ipad modal form presentations
        ((UIScrollView *)self.view).scrollEnabled = NO;
    }
        
    colors = [NSArray arrayWithObjects:[UIColor orangeColor], [UIColor brownColor],  [UIColor redColor],  [UIColor purpleColor], [UIColor darkGrayColor], [UIColor redColor],[UIColor greenColor],[UIColor yellowColor],[UIColor darkTextColor],nil];

    CGSize size = self.view.frame.size;
	
    CGRect tableFrame = CGRectMake(0.0f, 0.0f, size.width, size.height - INPUT_HEIGHT);
	self.tableView = [[UITableView alloc] initWithFrame:tableFrame style:UITableViewStylePlain];
	self.tableView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
	self.tableView.dataSource = self;
	self.tableView.delegate = self;
	[self.view addSubview:self.tableView];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(didTapOnTableView:)];
    [self.tableView addGestureRecognizer:tap];
	
    [self setBackgroundColor:[UIColor messagesBackgroundColor]];
    
    CGRect inputFrame = CGRectMake(0.0f, size.height - INPUT_HEIGHT - NAV_BAR_HEIGHT, size.width, INPUT_HEIGHT);
    self.inputView = [[MessageInputView alloc] initWithFrame:inputFrame delegate:self];
    
    UIButton *sendButton = [self sendButton];
    sendButton.enabled = NO;
    sendButton.frame = CGRectMake(self.inputView.frame.size.width - 65.0f, 8.0f, 59.0f, 26.0f);
    [sendButton addTarget:self
                   action:@selector(sendPressed:)
         forControlEvents:UIControlEventTouchUpInside];
    [self.inputView setSendButton:sendButton];
    [self.view addSubview:self.inputView];
    
    UISwipeGestureRecognizer *swipe = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(handleSwipe:)];
    swipe.direction = UISwipeGestureRecognizerDirectionDown;
    swipe.numberOfTouchesRequired = 1;
    [self.inputView addGestureRecognizer:swipe];

}

- (UIButton *)sendButton
{
    return [UIButton defaultSendButton];
}

#pragma mark - View lifecycle
- (void)viewDidLoad
{
    [super viewDidLoad];
    [self setup];
    //_inputView.backgroundColor = [UIColor redColor];
}

-(void) viewDidAppear:(BOOL)animated{
    _cancelKeyboardNotifications = NO;
}

- (void)viewWillAppear:(BOOL)animated
{
    [self scrollToBottomAnimated:NO];
    
	[[NSNotificationCenter defaultCenter] addObserver:self
											 selector:@selector(handleWillShowKeyboard:)
												 name:UIKeyboardWillShowNotification
                                               object:nil];
    
	[[NSNotificationCenter defaultCenter] addObserver:self
											 selector:@selector(handleWillHideKeyboard:)
												 name:UIKeyboardWillHideNotification
                                               object:nil];
}

- (void)viewWillDisappear:(BOOL)animated
{
	[[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
	[[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    NSLog(@"*** %@: didReceiveMemoryWarning ***", self.class);
}

#pragma mark - View rotation
- (BOOL)shouldAutorotate
{
    return NO;
}

- (NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}

- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    [super willRotateToInterfaceOrientation:toInterfaceOrientation duration:duration];
    [self.tableView reloadData];
    [self.tableView setNeedsLayout];
}

#pragma mark - Actions
- (void)sendPressed:(UIButton *)sender
{
    [self.delegate sendPressed:sender
                      withText:[self.inputView.textView.text trimWhitespace]];
}

- (void)handleSwipe:(UIGestureRecognizer *)guestureRecognizer
{
    [self.inputView.textView resignFirstResponder];
}

#pragma mark - Table view data source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 0;
}

-(void) didTapOnTableView:(UIGestureRecognizer*) recognizer {
    [self.view endEditing:YES];
    /*CGPoint tapLocation = [recognizer locationInView:self.tableView];
    NSIndexPath *indexPath = [self.tableView indexPathForRowAtPoint:tapLocation];
    
    if (indexPath) { //we are in a tableview cell, let the gesture be handled by the view
        recognizer.cancelsTouchesInView = NO;
    } else { // anywhere else, do what is needed for your case
        [self.navigationController popViewControllerAnimated:YES];
    }*/
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    BubbleMessageStyle style = [self.delegate messageStyleForRowAtIndexPath:indexPath];
    BOOL hasTimestamp = [self shouldHaveTimestampForRowAtIndexPath:indexPath];
    
    NSString *CellID = [NSString stringWithFormat:@"MessageCell_%d_%d", style, hasTimestamp];
    //NSString *CellID = [NSString stringWithFormat:@"MessageCell_%d", style];
    BubbleMessageCell *cell = (BubbleMessageCell *)[tableView dequeueReusableCellWithIdentifier:CellID];
    
    if(!cell) {
       cell = [[BubbleMessageCell alloc] initWithBubbleStyle:style
                                                   hasTimestamp:hasTimestamp
                                                reuseIdentifier:CellID];
        //cell = [[BubbleMessageCell alloc] initWithBubbleStyle:style reuseIdentifier:CellID];
    }
    
    NSDate *date = [self.dataSource timestampForRowAtIndexPath:indexPath];
    
    if(hasTimestamp)
        [cell setTimestamp:date];
    
    [cell setTime:date];
    
    NSString *userId = [[NSUserDefaults standardUserDefaults]
                        stringForKey:@"userId"];
    
    NSString *senderId = [self.dataSource senderIdForRowAtIndexPath:indexPath];
    NSString *userName = [self.dataSource userNameForRowAtIndexPath:indexPath];
    NSString *name = nil;
    if ([userId isEqualToString:senderId])
        name = [NSString stringWithFormat:@"%@", userName] ;
    else name = userName;
    
    [cell setUserName:name];
    UIColor *userColor = [self colorForUserId:senderId];
    [cell setUserColor:userColor];
    
    NSString *message = [self.dataSource textForRowAtIndexPath:indexPath];
    [cell setMessage:message];
    
    BOOL isAtPlace = [self.dataSource userAtPlaceAtIndexPath:indexPath];
    [cell setIsAtPlace:isAtPlace];
    
    BOOL received = [self.dataSource messageReceivedAtPlaceAtIndexPath:indexPath];
    [cell setIsReceived:received];

    [cell setBackgroundColor:tableView.backgroundColor];
    
    return cell;
}

-(UIColor *)colorForUserId:(NSString *)userId{
    if (colorPerUser==nil)
        colorPerUser = [NSMutableDictionary new];

    UIColor *userColor = [colorPerUser objectForKey:userId];
    
    if (userColor!=nil)
        return userColor;
    else {
        //NSLog(@"%@",userId);
        unsigned long count = [[colorPerUser allKeys] count];
        //NSLog(@"%lu",count);
        if (count==[colors count])
            return [UIColor orangeColor];
        userColor = [colors objectAtIndex:[[colorPerUser allKeys] count]];
        [colorPerUser setObject:userColor forKey:userId];
        return  userColor;
    }
}

#pragma mark - Table view delegate
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    //CGFloat dateHeight = [self shouldHaveTimestampForRowAtIndexPath:indexPath] ? DATE_LABEL_HEIGHT : 0.0f;
    NSString *userName = [self.dataSource userNameForRowAtIndexPath:indexPath];
    NSString *text = [self.dataSource textForRowAtIndexPath:indexPath];
    
    return [BubbleView cellHeightForText:text withUserName:userName];
}

#pragma mark - Messages view controller
- (BOOL)shouldHaveTimestampForRowAtIndexPath:(NSIndexPath *)indexPath
{
    switch ([self.delegate timestampPolicyForMessagesView]) {
        case MessagesViewTimestampPolicyAll:
            return YES;
            break;
        case MessagesViewTimestampPolicyAlternating:
            return indexPath.row % 2 == 0;
            break;
        case MessagesViewTimestampPolicyEveryThree:
            return indexPath.row % 3 == 0;
            break;
        case MessagesViewTimestampPolicyEveryFive:
            return indexPath.row % 5 == 0;
            break;
        case MessagesViewTimestampPolicyCustom:
            return [self.delegate hasTimestampForRowAtIndexPath:indexPath];
            break;
    }
    
    return NO;
}

- (void)finishSend
{
    [self.inputView.textView setText:nil];
    [self textViewDidChange:self.inputView.textView];
    [self.tableView reloadData];
    [self scrollToBottomAnimated:YES];
}

- (void)setBackgroundColor:(UIColor *)color
{
    self.view.backgroundColor = color;
    self.tableView.backgroundColor = color;
    self.tableView.separatorColor = color;
}

- (void)scrollToBottomAnimated:(BOOL)animated
{
    NSInteger rows = [self.tableView numberOfRowsInSection:0];
    
    if(rows > 0) {
        [self.tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:rows - 1 inSection:0]
                              atScrollPosition:UITableViewScrollPositionBottom
                                      animated:animated];
    }
}

#pragma mark - Text view delegate
- (void)textViewDidBeginEditing:(UITextView *)textView
{
    [textView becomeFirstResponder];
    
    if(!self.previousTextViewContentHeight){
        //NSLog(@"self.inputView.textView.frame.size.height: %f",self.inputView.textView.contentSize.height);
        //NSLog(@"textView.contentSize.height: %f",textView.contentSize.height);
        self.previousTextViewContentHeight = textView.contentSize.height;
    }
    
    [self scrollToBottomAnimated:YES];
}

- (void)textViewDidEndEditing:(UITextView *)textView
{
    [textView resignFirstResponder];
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text{
   // NSLog(@"text %lu", (unsigned long)text.length);
    //NSLog(@"text length %lu", (unsigned long)textView.text.length);

    if (textView.text.length==70 && text.length==1)
        return NO;
    else return YES;
}

- (void)textViewDidChange:(UITextView *)textView{        
    CGFloat maxHeight = [MessageInputView maxHeight];
    CGFloat textViewContentHeight = [textView sizeThatFits:CGSizeMake(textView.contentSize.width, FLT_MAX)].height;
    BOOL isShrinking = textViewContentHeight < self.previousTextViewContentHeight;
    CGFloat changeInHeight = textViewContentHeight - self.previousTextViewContentHeight;
    
    changeInHeight = (textViewContentHeight + changeInHeight >= maxHeight) ? 0.0f : changeInHeight;
    
    if(!isShrinking)
        [self.inputView adjustTextViewHeightBy:changeInHeight];
    
    if(changeInHeight != 0.0f) {
        [UIView animateWithDuration:0.25f
                         animations:^{
                             UIEdgeInsets insets = UIEdgeInsetsMake(0.0f, 0.0f, self.tableView.contentInset.bottom + changeInHeight, 0.0f);
                             self.tableView.contentInset = insets;
                             self.tableView.scrollIndicatorInsets = insets;
                            
                             [self scrollToBottomAnimated:NO];
                            
                             CGRect inputViewFrame = self.inputView.frame;
                             self.inputView.frame = CGRectMake(0.0f,
                                                               inputViewFrame.origin.y - changeInHeight,
                                                               inputViewFrame.size.width,
                                                               inputViewFrame.size.height + changeInHeight);
                         }
                         completion:^(BOOL finished) {
                             if(isShrinking)
                                 [self.inputView adjustTextViewHeightBy:changeInHeight];
                         }];
        
        self.previousTextViewContentHeight = MIN(textViewContentHeight, maxHeight);
    }
    
    self.inputView.sendButton.enabled = !self.chatIsReadOnly && ([textView.text trimWhitespace].length > 0);
}

#pragma mark - Keyboard notifications
- (void)handleWillShowKeyboard:(NSNotification *)notification
{
    if (!_cancelKeyboardNotifications){
        [self keyboardWillShowHide:notification];
    }
}

- (void)handleWillHideKeyboard:(NSNotification *)notification
{
    if (!_cancelKeyboardNotifications){
        [self keyboardWillShowHide:notification];
    }
}

- (void)keyboardWillShowHide:(NSNotification *)notification
{
    CGRect keyboardRect = [[notification.userInfo objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue];
	UIViewAnimationCurve curve = [[notification.userInfo objectForKey:UIKeyboardAnimationCurveUserInfoKey] integerValue];
	double duration = [[notification.userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey] doubleValue];
    
    [UIView animateWithDuration:duration
                          delay:0.0f
                        options:[UIView animationOptionsForCurve:curve]
                     animations:^{
                         CGFloat keyboardY = [self.view convertRect:keyboardRect fromView:nil].origin.y;
                         
                         CGRect inputViewFrame = self.inputView.frame;
                         CGFloat inputViewFrameY = keyboardY - inputViewFrame.size.height;
                         
                         // for ipad modal form presentations
                         CGFloat messageViewFrameBottom = self.view.frame.size.height - INPUT_HEIGHT - NAV_BAR_HEIGHT;
                         if(inputViewFrameY > messageViewFrameBottom)
                             inputViewFrameY = messageViewFrameBottom;

                         self.inputView.frame = CGRectMake(inputViewFrame.origin.x,
                                                           inputViewFrameY,
                                                           inputViewFrame.size.width,
                                                           inputViewFrame.size.height);
                         
                         UIEdgeInsets insets = UIEdgeInsetsMake(65.0f,
                                                                0.0f,
                                                                self.view.frame.size.height - self.inputView.frame.origin.y - INPUT_HEIGHT,
                                                                0.0f);
                         
                         self.tableView.contentInset = insets;
                         self.tableView.scrollIndicatorInsets = insets;
                     }
                     completion:^(BOOL finished) {
                     }];
}

@end