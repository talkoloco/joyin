#import <UIKit/UIKit.h>

@interface UIButton (MessagesView)

+ (UIButton *)defaultSendButton;

@end