#import "UIColor+MessagesView.h"

@implementation UIColor (MessagesView)

+ (UIColor *)messagesBackgroundColor
{
    //return [UIColor colorWithRed:0.859f green:0.886f blue:0.929f alpha:1.0f];
    return [UIColor whiteColor];
}

+ (UIColor *)messagesTimestampColor
{
    return [UIColor colorWithRed:0.533f green:0.573f blue:0.647f alpha:1.0f];
}

+ (UIColor *)messagesUserNameColor
{
    return [UIColor colorWithRed:0.533f green:0.573f blue:0.647f alpha:1.0f];
}

@end