#import "TalksViewController.h"
#import "Group.h"
#import <MapKit/MapKit.h>
#import "GlossyButton.h"
#import "TalkolocoUtils.h"


@interface ChatViewController : TalksViewController <MessagesViewDelegate, MessagesViewDataSource, CLLocationManagerDelegate, UIAlertViewDelegate>{
    BOOL byPush;
    CLLocation *userLocation;
    CLLocationManager *locationManager;
    int deltaInSeconds;
    int chunk;
    int chunkDelta;
    UIButton *moreBtn;
    UIView *container;
    UIView *bottomView;
}

@property (strong, nonatomic) NSMutableArray *messages;
@property (strong, nonatomic) NSMutableArray *senderIds;
@property (strong, nonatomic) NSMutableArray *timestamps;
@property (strong, nonatomic) NSMutableArray *userNames;
@property (strong, nonatomic) NSMutableArray *usersIsAtPlace;
@property (strong, nonatomic) NSMutableArray *messagesReceived;
@property (strong, nonatomic) Group *group;
@property (readwrite, nonatomic) BOOL readOnly;
@property (strong, nonatomic) UIActivityIndicatorView *activityIndicator;
//@property (strong, nonatomic) UIBarButtonItem *joinButton;
@property (strong, nonatomic) UIBarButtonItem *unFollowButton;

@property (strong, nonatomic) UIBarButtonItem *activityButton;
@property (strong, nonatomic) UILabel *followerLable;
@property (strong, nonatomic) MKMapView *mapView;
@property (readwrite, nonatomic) BOOL isAtPlace;

-(id)initByPush;

@end