
#import "BubbleMessageCell.h"
#import "UIColor+MessagesView.h"
#import "TalkolocoUtils.h"

@interface BubbleMessageCell()

@property (strong, nonatomic) BubbleView *bubbleView;
@property (strong, nonatomic) UILabel *timestampLabel;
@property (strong, nonatomic) UILabel *userNameLabel;
@property (strong, nonatomic) UIColor *userColor;

- (void)setup;
- (void)configureTimestampLabel;
- (void)configureUserNameLabel;
- (void)configureWithStyle:(BubbleMessageStyle)style timestamp:(BOOL)hasTimestamp;
- (void)configureWithStyle:(BubbleMessageStyle)style;


@end



@implementation BubbleMessageCell

#pragma mark - Initialization
- (void)setup
{
    self.backgroundColor = [UIColor clearColor];
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    self.accessoryType = UITableViewCellAccessoryNone;
    self.accessoryView = nil;
    
    self.imageView.image = nil;
    self.imageView.hidden = YES;
    self.textLabel.text = nil;
    self.textLabel.hidden = YES;
    self.detailTextLabel.text = nil;
    self.detailTextLabel.hidden = YES;
}

- (void)configureUserNameLabel{
    self.userNameLabel = [[UILabel alloc] initWithFrame:CGRectMake(0.0f,
                                                                    4.0f,
                                                                    self.bounds.size.width,
                                                                    14.5f)];
    self.userNameLabel.autoresizingMask =  UIViewAutoresizingFlexibleWidth;
    self.userNameLabel.backgroundColor = [UIColor clearColor];
    self.userNameLabel.textAlignment = NSTextAlignmentCenter;
    self.userNameLabel.textColor = [UIColor messagesUserNameColor];
    self.userNameLabel.shadowColor = [UIColor whiteColor];
    self.userNameLabel.shadowOffset = CGSizeMake(0.0f, 1.0f);
    self.userNameLabel.font = [UIFont boldSystemFontOfSize:11.5f];
    //self.userNameLabel.font = [UIFont fontWithName:FONT size:30];
    //self.userNameLabel.text = @"sdlfjsldfjlskdfjlksdjfkl";
    
    [self.contentView addSubview:self.userNameLabel];
    [self.contentView bringSubviewToFront:self.userNameLabel];

}


- (void)configureTimestampLabel
{
    self.timestampLabel = [[UILabel alloc] initWithFrame:CGRectMake(0.0f,
                                                                    4.0f,
                                                                    self.bounds.size.width,
                                                                    14.5f)];
    self.timestampLabel.autoresizingMask =  UIViewAutoresizingFlexibleWidth;
    self.timestampLabel.backgroundColor = [UIColor clearColor];
    self.timestampLabel.textAlignment = NSTextAlignmentCenter;
    self.timestampLabel.textColor = [UIColor messagesTimestampColor];
    self.timestampLabel.shadowColor = [UIColor whiteColor];
    self.timestampLabel.shadowOffset = CGSizeMake(0.0f, 1.0f);
    self.timestampLabel.font = [UIFont boldSystemFontOfSize:11.5f];
    
    [self.contentView addSubview:self.timestampLabel];
    [self.contentView bringSubviewToFront:self.timestampLabel];
}

- (void)configureWithStyle:(BubbleMessageStyle)style timestamp:(BOOL)hasTimestamp
{
    CGFloat bubbleY = 7.0f;
    
    if(hasTimestamp) {
        [self configureTimestampLabel];
        bubbleY = 14.0f;
    }
    
    CGRect frame = CGRectMake(0.0f,
                              bubbleY,
                              self.contentView.frame.size.width,
                              self.contentView.frame.size.height - self.timestampLabel.frame.size.height);
    
    self.bubbleView = [[BubbleView alloc] initWithFrame:frame
                                              bubbleStyle:style];
    
    self.bubbleView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    
    [self.contentView addSubview:self.bubbleView];
    [self.contentView sendSubviewToBack:self.bubbleView];
}

- (void)configureWithStyle:(BubbleMessageStyle)style{
    CGFloat bubbleY = 7.0f;
    
    //[self configureUserNameLabel];
    
    
    
    CGRect frame = CGRectMake(0.0f,
                              bubbleY,
                              self.contentView.frame.size.width,
                              self.contentView.frame.size.height);
    
    self.bubbleView = [[BubbleView alloc] initWithFrame:frame
                                            bubbleStyle:style];
    
    self.bubbleView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    
    [self.contentView addSubview:self.bubbleView];
    [self.contentView sendSubviewToBack:self.bubbleView];

}

- (id)initWithBubbleStyle:(BubbleMessageStyle)style hasTimestamp:(BOOL)hasTimestamp reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:UITableViewCellStyleDefault reuseIdentifier:reuseIdentifier];
    if(self) {
        [self setup];
        [self configureWithStyle:style timestamp:hasTimestamp];
    }
    return self;
}

- (id)initWithBubbleStyle:(BubbleMessageStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:UITableViewCellStyleDefault reuseIdentifier:reuseIdentifier];
    if(self) {
        [self setup];
        [self configureWithStyle:style];
    }
    return self;
}


#pragma mark - Setters
- (void)setBackgroundColor:(UIColor *)color
{
    [super setBackgroundColor:color];
    [self.contentView setBackgroundColor:color];
    [self.bubbleView setBackgroundColor:color];
}

#pragma mark - Message Cell
- (void)setMessage:(NSString *)msg
{
    self.bubbleView.text = msg;
}

-(void)setUserColor:(UIColor *)color{
    self.bubbleView.userColor = color;
}

-(void)setIsAtPlace:(BOOL)isAtPlace{
    self.bubbleView.isAtPlace = isAtPlace;
}


-(void)setIsReceived:(BOOL)isReceived{
    self.bubbleView.isReceived = isReceived;
}

- (void)setTime:(NSDate *)date{
    self.bubbleView.time = [NSDateFormatter localizedStringFromDate:date
                                                               dateStyle:(NSDateFormatterStyle)kCFDateFormatterNoStyle
                                                               timeStyle:NSDateFormatterShortStyle];
    
    if (self.bubbleView.time.length==7 || self.bubbleView.time.length==8)
        self.bubbleView.time = [self.bubbleView.time substringToIndex:self.bubbleView.time.length - 3];

    //NSLog(@"time %@",self.bubbleView.time);
}


- (void)setTimestamp:(NSDate *)date
{
    self.timestampLabel.text = [NSDateFormatter localizedStringFromDate:date
                                                              dateStyle:NSDateFormatterMediumStyle
                                                              timeStyle:NSDateFormatterShortStyle];
}

- (void)setUserName:(NSString *)userName{
    self.userNameLabel.text = userName;
    self.bubbleView.userName = userName;
}


@end