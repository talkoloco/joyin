#import <UIKit/UIKit.h>

@interface UIView (AnimationOptionsForCurve)

+ (UIViewAnimationOptions)animationOptionsForCurve:(UIViewAnimationCurve)curve;

@end