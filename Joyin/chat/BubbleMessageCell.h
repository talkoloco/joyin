#import <UIKit/UIKit.h>
#import "BubbleView.h"

#define DATE_LABEL_HEIGHT 12.0f
#define USER_NAME_LABEL_HEIGHT 12.0f


@interface BubbleMessageCell : UITableViewCell

#pragma mark - Initialization
- (id)initWithBubbleStyle:(BubbleMessageStyle)style
             hasTimestamp:(BOOL)hasTimestamp
          reuseIdentifier:(NSString *)reuseIdentifier;

- (id)initWithBubbleStyle:(BubbleMessageStyle)style reuseIdentifier:(NSString *)reuseIdentifier;

#pragma mark - Message Cell
- (void)setMessage:(NSString *)msg;
- (void)setTimestamp:(NSDate *)date;
- (void)setTime:(NSDate *)date;
- (void)setUserName:(NSString *)userName;
-(void)setUserColor:(UIColor *)color;
-(void)setIsAtPlace:(BOOL)isAtPlace;
-(void)setIsReceived:(BOOL)isReceived;



@end