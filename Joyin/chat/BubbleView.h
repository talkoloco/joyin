#import <UIKit/UIKit.h>

typedef enum {
	BubbleMessageStyleIncomingDefault = 0,
    BubbleMessageStyleIncomingSquare,
    BubbleMessageStyleOutgoingDefault,
	BubbleMessageStyleOutgoingDefaultGreen,
    BubbleMessageStyleOutgoingSquare
} BubbleMessageStyle;



@interface BubbleView : UIView

@property (assign, nonatomic) BubbleMessageStyle style;
@property (copy, nonatomic) NSString *text;
@property (copy, nonatomic) NSString *userName;
@property (copy, nonatomic) UIColor *userColor;
@property (nonatomic, readwrite) BOOL isAtPlace;
@property (nonatomic, readwrite) BOOL isReceived;
@property (copy, nonatomic) NSString *time;
@property (assign, nonatomic) UIImage *lastCellImage;


#pragma mark - Initialization
- (id)initWithFrame:(CGRect)frame bubbleStyle:(BubbleMessageStyle)bubbleStyle;

#pragma mark - Bubble view
+ (UIImage *)bubbleImageForStyle:(BubbleMessageStyle)style;
+ (UIFont *)font;
+ (CGSize)textSizeForText:(NSString *)txt;
+ (CGSize)bubbleSizeForText:(NSString *)txt withUserName:(NSString *)userName;
+ (CGFloat)cellHeightForText:(NSString *)txt withUserName:(NSString *)userName;
+ (int)maxCharactersPerLine;
+ (int)numberOfLinesForMessage:(NSString *)txt;

@end