#import <Foundation/Foundation.h>

@interface NSString (MessagesView)

- (NSString *)trimWhitespace;
- (NSUInteger)numberOfLines;

@end