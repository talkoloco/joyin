
#import <UIKit/UIKit.h>

@interface UIColor (MessagesView)

+ (UIColor *)messagesBackgroundColor;
+ (UIColor *)messagesTimestampColor;
+ (UIColor *)messagesUserNameColor;




@end