//
//  HasTimestamp.h
//  Talkoloco
//
//  Created by Ohad on 2014-04-02.
//  Copyright (c) 2014 Talkoloco. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface HasTimestamp : NSObject

@property (assign, nonatomic) NSInteger deltaInSeconds;
@property (assign, nonatomic) NSInteger sumDeltaInSeconds;
@property (assign, nonatomic) BOOL hasTimestamp;


@end
