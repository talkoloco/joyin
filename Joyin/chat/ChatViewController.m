#import "ChatViewController.h"
//#import "Flurry.h"
#import "TalkolocoUtils.h"
//#import "CreateGroupViewController.h"
#import <FacebookSDK/FacebookSDK.h>
#import "NSString+JRStringAdditions.h"
#import "UIView+Expanded.h"
#import "HasTimestamp.h"
#import "AYUIButton.h"

#define CHUNK_SIZE 15

@interface ChatViewController (){
    UIView *titleView;
    UILabel *titleLable;
    NSMutableDictionary *shouldHaveTimestamp;

}
@end

@implementation ChatViewController{
    //UIImageView *createArrow;
    //UIButton *inviteBtn;
}

-(id)initByPush {
	if( (self = [self init]) ) {
        byPush = YES;
	}
	return self;
}

-(id)initWithCoder:(NSCoder *)aDecoder{
    if(self = [super initWithCoder:aDecoder]){
        [self registerToRemotireNotifications];
    }
    return self;
}



-(id)init{
    if(self = [super init]){
        self.hidesBottomBarWhenPushed = YES;
        [self registerToRemotireNotifications];
        locationManager = [[CLLocationManager alloc] init];
        locationManager.delegate = self;
        locationManager.desiredAccuracy = kCLLocationAccuracyBestForNavigation;
        deltaInSeconds = 0;
        //shouldHaveTimestamp = [NSMutableDictionary new];
        if ([[TalkolocoUtils sharedInstance] isPhone5])
            chunkDelta = CHUNK_SIZE;
        else
            chunkDelta = CHUNK_SIZE - 1;
        chunk = chunkDelta;
    }
    return self;
}

- (void)locationManager:(CLLocationManager *)manager
	 didUpdateLocations:(NSArray *)locations{
    CLLocation *location = [locations objectAtIndex:0];
    userLocation = location;
    
    if (_group!=nil)
        [self updateIfUserIsAtPlace: _group];
    [self performSelector:@selector(stopUpdatingLocation) withObject:nil afterDelay:3];
    
   // NSLog(@"latitude: %f, longitude: %f",userLocation.coordinate.latitude,userLocation.coordinate.longitude);
}

-(void)stopUpdatingLocation{
    [[TalkolocoUtils sharedInstance] saveUserLocation:userLocation];
    [locationManager stopUpdatingLocation];
}


-(void)registerToRemotireNotifications{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(remotePushNotification) name:@"RemotePushNotification" object:nil];
}

-(void)remotePushNotification{
    //[inviteBtn removeFromSuperview];
    //[createArrow removeFromSuperview];
    [self loadGroupMessages : [[NSNumber alloc] initWithBool:YES]];
    [MessageSoundEffect playMessageReceivedSound];
}

#pragma mark - Initialization
- (UIButton *)sendButton
{
    // Override to use a custom send button
    // The button's frame is set automatically for you
    return [UIButton defaultSendButton];
}

-(Group *)group{
    return _group;
}

-(NSMutableArray *)timestamps{
    if(!_timestamps){
        _timestamps = [ NSMutableArray new];
    }
    return _timestamps;
}

-(NSMutableArray *)messages{
    if(!_messages){
        _messages  = [ NSMutableArray new];
    }
    return _messages;
}


-(NSMutableArray *)senderIds{
    if(!_senderIds){
        _senderIds  = [ NSMutableArray new];
    }
    return _senderIds;
}

-(NSMutableArray *)userNames{
    if(!_userNames){
        _userNames  = [ NSMutableArray new];
    }
    return _userNames;
}

- (void)viewDidDisappear:(BOOL)animated{
    [[NSUserDefaults standardUserDefaults] setObject:@"NO" forKey:@"ChatVisible"];
   // [[NSUserDefaults standardUserDefaults] setObject:@"None" forKey:@"ChatGroupId"];
}

/*
- (void)didReceiveMemoryWarning{
    [super didReceiveMemoryWarning];
    // unregister for keyboard notifications while not visible.
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillShowNotification
                                                  object:nil];
    // unregister for keyboard notifications while not visible.
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillHideNotification
                                                  object:nil];
}*/



-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    [locationManager startUpdatingLocation];
    
   /* if (!createArrow ){
        createArrow = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"arrow.png"]];
        createArrow.frame = CGRectMake(10, self.view.frame.size.height - 200, 180, 180);
        createArrow.alpha = 0.0;
        createArrow.hidden = YES;
        [self.view addSubview:createArrow];
    }*/
    
   /* if (!inviteBtn){
        inviteBtn = [UIButton new];
        inviteBtn.alpha = 0.0;
        inviteBtn.hidden = YES;

        int width = 200;
        int middle = self.view.frame.size.width / 2 - width /2;
        int y = self.view.frame.size.height / 2;
        
        if ([[TalkolocoUtils sharedInstance] isPhone5]){
            y = y + 50;
        }
        
        int height = 50;
        inviteBtn.frame = CGRectMake(middle, y, width, height);
        [inviteBtn setTitle:@"Invite Friends" forState:UIControlStateNormal];
        inviteBtn.layer.borderWidth=1.0f;
        inviteBtn.layer.cornerRadius = 4;
        inviteBtn.layer.borderColor = [[UIColor grayColor] CGColor];
        [inviteBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        UIColor *color = [UIColor colorWithRed:205/255.0 green:201/255.0 blue:201/255.0 alpha:1];
        [inviteBtn setTitleColor:color forState:UIControlStateSelected];
        [inviteBtn setTitleColor:color forState:UIControlStateHighlighted];
        [inviteBtn.titleLabel setFont:[UIFont fontWithName:FONT size:18]];
        
        [inviteBtn addTarget:self action:@selector(manageGroup) forControlEvents:UIControlEventTouchUpInside];
        
        [self.view addSubview:inviteBtn];
    }*/
    
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [[NSUserDefaults standardUserDefaults] setObject:@"YES" forKey:@"ChatVisible"];
    [[NSUserDefaults standardUserDefaults] setObject:_group.groupId forKey:@"ChatGroupId"];
}

-(void)manageGroup{
    /*UIStoryboard *sb = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    CreateGroupViewController *createGroupViewController = [sb instantiateViewControllerWithIdentifier:@"createGroupViewController"];
    createGroupViewController.group = _group;
    [self.navigationController pushViewController:createGroupViewController animated:YES];*/
}

-(void)enableSendButton{
    super.inputView.sendButton.enabled = YES;
    super.chatIsReadOnly = NO;

}

-(void)disableSendButton{
    super.inputView.sendButton.enabled = NO;
    super.chatIsReadOnly = YES;
}

- (void)navigationBarDoubleTap:(UIGestureRecognizer*)recognizer {
   /* UIStoryboard *sb = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    CreateGroupViewController *createGroupViewController = [sb instantiateViewControllerWithIdentifier:@"createGroupViewController"];
    createGroupViewController.group = _group;
    [self.navigationController pushViewController:createGroupViewController animated:YES];*/
}

-(void)buildTitleButton: (NSString *)title{
    
    titleView = [UIView new];
    titleView.frame = CGRectMake(130, 5, 100, 100);
    
    UIButton *titleBtn = [UIButton new];
    [titleBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [titleBtn setTitle:title forState:UIControlStateNormal];
//    if (!_group.isPrivate){
    [titleBtn addTarget:self action:@selector(navigationBarDoubleTap:) forControlEvents:UIControlEventTouchUpInside];
    [titleBtn setTitleColor:[UIColor colorWithRed:0 green:0 blue:0 alpha:0.5] forState:UIControlStateHighlighted];

  //  }
    titleBtn.titleLabel.font = [UIFont boldSystemFontOfSize:18];
    [titleBtn sizeToFit];
    
    _activityIndicator = [[UIActivityIndicatorView alloc] initWithFrame:CGRectMake(titleBtn.frame.size.width + 5, 7, 20, 20)];
    _activityIndicator.color = [UIColor blackColor];
    [_activityIndicator startAnimating];
    
    [titleView addSubview: titleBtn];
    [titleView addSubview: _activityIndicator];
    [titleView resizeToFitSubviews];


    self.navigationItem.titleView = titleView;
}

#pragma mark - View lifecycle
- (void)viewDidLoad
{
    [super viewDidLoad];
  //  [Flurry logPageView];

    CGRect rect = self.view.frame;
    rect.size.height = rect.size.height - 150;
    self.view.frame = rect;
    
    self.delegate = self;
    self.dataSource = self;
    
    //self.activityIndicator = [[UIActivityIndicatorView alloc] initWithFrame:CGRectMake(0, 0, 20, 20)];
    //self.activityIndicator.color = [UIColor blackColor];
    
    /*self.joinButton = [[UIBarButtonItem alloc] initWithTitle:@"Join"
                                                                style:UIBarButtonItemStyleDone target:self
                                                               action:@selector(addUserToGroup)];*/
    
    self.unFollowButton = [[UIBarButtonItem alloc] initWithTitle:@"Unfollow"
                                                       style:UIBarButtonItemStyleDone target:self
                                                      action:@selector(unfollowGroup)];
 //   self.activityButton =
   // [[UIBarButtonItem alloc] initWithCustomView:self.activityIndicator];
    
    // Set to Left or Right
   // [[self navigationItem] setRightBarButtonItem:self.activityButton];
    
   // [self.activityIndicator startAnimating];

    
    if (byPush){
        [self loadGroupDetails:_group.groupId];
    }else{
        [self buildGroupView];
        //[self.activityIndicator stopAnimating];
    }
    

    
}

- (void)handleWillShowKeyboard:(NSNotification *)notification{
    [super handleWillShowKeyboard:notification];
    [self keyboardWillShow: notification];
}

- (void)handleWillHideKeyboard:(NSNotification *)notification{
    [super handleWillShowKeyboard:notification];
    [self keyboardWillHide: notification];
}



- (void)keyboardWillHide:(NSNotification*)notification{
  /*  if ([_messages count]==0){
        createArrow.hidden = NO;
        inviteBtn.hidden = NO;
    }*/
}

- (void)keyboardWillShow:(NSNotification*)notification{
   /* if ([_messages count]==0){
        createArrow.hidden = YES;
        inviteBtn.hidden = YES;
    }*/
}

-(void)buildGroupView{
    [self buildHeader];
    [self updateIfUserIsAtPlace: _group];
    [self buildTitleButton:_group.groupName];
    [self loadGroupMessages];
    
}

-(void)updateIfUserIsAtPlace:(Group *)group{
    double radius = [RADIUS doubleValue];
    /*NSLog(@"%f",group.latitude);
    NSLog(@"%f",userLocation.coordinate.latitude);
    NSLog(@"%f",group.longitude);
    NSLog(@"%f",userLocation.coordinate.longitude);*/
    double d = sqrt(pow(group.latitude - userLocation.coordinate.latitude,2)+pow(group.longitude -userLocation.coordinate.longitude,2));   //d = SQRT((x2-x1)2+(y2-y1)2)
    if (d<radius)
        self.isAtPlace = YES;
    else
        self.isAtPlace = NO;
}

-(void)loadGroupDetails:(NSString *)groupId{
    //[Flurry logEvent:@"loading_group_details" timed:YES];
    NSString *post = [NSString stringWithFormat:@"{\"groupId\":\"%@\"}",groupId];
   // NSLog(@"%@",post);
    NSData *postData = [post dataUsingEncoding:NSUTF8StringEncoding];
    NSString *urlStr = [NSString stringWithFormat:@"http://%@:%@/groupById",IPADDRESS, PORT];
    NSMutableURLRequest *request = [[TalkolocoUtils sharedInstance]  buildRequest:urlStr withPostData:postData];
    
    NSOperationQueue *queue = [[NSOperationQueue alloc] init];
    [NSURLConnection sendAsynchronousRequest:request queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
     {
         if ([data length] > 0 && error == nil){
             [self receiveGroupData:data];
             [self performSelectorOnMainThread:@selector(buildGroupView) withObject:nil waitUntilDone:NO];
         } else {
             NSLog(@"Error: %@", [error description]);
         }
        // [self performSelectorOnMainThread:@selector(stopLoading) withObject:nil waitUntilDone:NO];
         //[Flurry endTimedEvent:@"loading_group_details" withParameters:nil];
     }];

}

-(void)receiveGroupData:(NSData *)data{
    //NSString *str=[[NSString alloc]initWithData:data encoding:NSUTF8StringEncoding];
  //  NSLog(str, @"%@");
    NSDictionary *groupItem=[NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
    Group *group = [[TalkolocoUtils sharedInstance] parseResponseToGroup:groupItem];
    self.group = group;
    CLLocation *location;
    location = [[CLLocation alloc] initWithLatitude:group.latitude longitude:group.longitude];
    [self performSelectorOnMainThread:@selector(zoomToPlace:) withObject:location waitUntilDone:NO];
    //[self performSelectorOnMainThread:@selector(buildTitleButton:) withObject:group.groupName waitUntilDone:NO];
}

-(void)zoomToPlace:(CLLocation *)location{
    MKCoordinateRegion region;
    region.center = location.coordinate;
    MKCoordinateSpan span;
    span.latitudeDelta = 0.003;
    span.longitudeDelta = 0.003;
    region.span = span;
    [self addPlacemarkAnnotationToMap:_group];
    [self.mapView setRegion:region animated:YES];
}

- (void)addPlacemarkAnnotationToMap:(Group *)group{
    MKPointAnnotation *selectedPlaceAnnotation = [[MKPointAnnotation alloc] init];
    CLLocation *location = [[CLLocation alloc] initWithLatitude:group.latitude longitude:group.longitude];
    selectedPlaceAnnotation.coordinate = location.coordinate;
    selectedPlaceAnnotation.title = [NSString stringWithFormat:@"%@",group.address];
    [self.mapView addAnnotation:selectedPlaceAnnotation];
}

-(void)buildFollowerLable{
    
    self.followerLable = [UILabel new];
    
    //self.followerLable.frame = CGRectMake(82, 82, 105, 40);
    
    self.followerLable.frame = CGRectMake(self.view.frame.size.width - 75 ,50,70,70);
    _followerLable.alpha = 0.6;
    self.followerLable.layer.cornerRadius = 35;
    
    UIColor *color = [UIColor
                      colorWithRed:255/255.0
                      green:124.0/255.0
                      blue:25/255.0
                      alpha:1];
    
    self.followerLable.textColor = [UIColor whiteColor];
    self.followerLable.backgroundColor = color;//[UIColor clearColor];
	//self.headerLabel.backgroundColor =  color;
    self.followerLable.textAlignment = NSTextAlignmentCenter;
    
   // [self.followerLable.layer setCornerRadius:kViewRoundedCornerRadius];
    
    self.followerLable.font = [UIFont fontWithName: FONT_BOLD size:35.0];
    
    self.followerLable.text = [NSString stringWithFormat:@"%lu", (unsigned long)_group.size];
}

-(void)loadMoreBtn{
    CGRect rect;
    if (!_group.isPrivate){
        rect = CGRectMake(5, 135, self.view.frame.size.width-10, 35);
    } else {
        rect = CGRectMake(5, 5, self.view.frame.size.width-10, 35);
    }
    moreBtn = [[UIButton alloc] initWithFrame:rect];
    moreBtn = [UIButton buttonWithType:UIButtonTypeSystem];
    moreBtn.frame  = rect;
    moreBtn.hidden = YES;
    //[[startBtn layer] setBorderWidth:0.3f];
    //[[startBtn layer] setBorderColor:[UIColor lightGrayColor].CGColor];
    [moreBtn setTitle:@"Load Earlier" forState:UIControlStateNormal];
    moreBtn.titleLabel.font = [UIFont fontWithName: FONT size:16.0];
    [moreBtn addTarget:self action:@selector(loadEarlier) forControlEvents:UIControlEventTouchUpInside];
    [container addSubview:moreBtn];
}

-(void)loadEarlier{
    moreBtn.userInteractionEnabled = NO;
    self.tableView.scrollEnabled = NO;
    if (![self.activityIndicator isAnimating]){
        [[self navigationItem] setRightBarButtonItem:self.activityButton];
        [self.activityIndicator startAnimating];
        chunk+=chunkDelta;
        [self loadGroupMessages:[[NSNumber alloc] initWithBool:NO]];
    }
}

-(void)buildHeader{
    if (!_group.isPrivate){
        [self buildMapView];
    }
    
    container = [UIView new];
    if (!_group.isPrivate){
        container.frame = CGRectMake(0, 0, self.view.frame.size.width, 170);
        [container addSubview:self.mapView];
        [container addSubview:[self bottomBorder]];
    } else {
        container.frame = CGRectMake(0, 0, self.view.frame.size.width, 5);
    }
    [self loadMoreBtn];
    
	super.tableView.tableHeaderView = container;
}

-(UIView *)bottomBorder{
    bottomView = [UIView new];
    bottomView.frame = CGRectMake(0, 158, self.view.frame.size.width, 2);
    bottomView.backgroundColor = [UIColor clearColor];
    
    UIColor *offWhite = [UIColor
                         colorWithRed:250/255.0
                         green:235/255.0
                         blue:215/255.0
                         alpha:0.9];
    
    CAGradientLayer *gradientHeader = [CAGradientLayer layer];
    gradientHeader.frame = bottomView.bounds;
    gradientHeader.colors = [NSArray arrayWithObjects:(id)[offWhite CGColor], (id)[[UIColor lightGrayColor] CGColor],nil];
    [bottomView.layer insertSublayer:gradientHeader atIndex:0];
    return bottomView;
}

-(void)buildMapView{
    CGSize result = [[UIScreen mainScreen] bounds].size;
    if(result.height == 480)
    {
        self.mapView = [[MKMapView alloc] initWithFrame:CGRectMake(0, 2, self.view.frame.size.width, 158)];
    }
    if(result.height == 568)
    {
        self.mapView = [[MKMapView alloc] initWithFrame: CGRectMake(0, 2, self.view.frame.size.width, 158)];
    }
    
    _mapView.autoresizingMask =  UIViewAutoresizingFlexibleWidth;
    
    //[self.mapView.layer setCornerRadius:kViewRoundedCornerRadius];

    self.mapView.showsUserLocation = YES;
    
    //[self.mapView.layer setCornerRadius:kViewRoundedCornerRadius];
    
    CLLocation *location = [[CLLocation alloc] initWithLatitude:_group.latitude longitude:_group.longitude];
    [self zoomToPlace:location];
}

-(void)unfollowGroup{
    if (_group!=nil && _group.groupId!=nil){
        
        NSString *userId = [[NSUserDefaults standardUserDefaults]
                            stringForKey:@"userId"];
        
        
        if ([_group.members containsObject:userId] && [_group.members count]==1){
            UIAlertView *message = [[UIAlertView alloc] initWithTitle:@"Warning"
                                                              message:@"You are about to delete this place"
                                                             delegate:self
                                                    cancelButtonTitle:@"OK"
                                                    otherButtonTitles:@"Cancel", nil];
            [message show];
        } else {
            [self removeUserFrounGroup];
        }
    }
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (buttonIndex==0){
        [self removeUserFrounGroup];
    }
}

-(void)removeUserFrounGroup{
    
    [titleView addSubview:_activityIndicator];
    [_activityIndicator startAnimating];
    
    NSString *userId = [[NSUserDefaults standardUserDefaults]
                        stringForKey:@"userId"];
    
    [[self navigationItem] setRightBarButtonItem:self.activityButton];
    [self.activityIndicator startAnimating];
    
    NSMutableArray *members = [[NSMutableArray alloc] initWithArray:_group.members];
    
    
    [members removeObject:userId];
    _group.members = [members copy];
    _group.size = [_group.members count];
    [self removeUserFromGroup: _group.groupId withContext:self];
}

-(void)removeUserFromGroup:(NSString *)groupId withContext:(UIViewController *)controller{
    NSString *userId = [[NSUserDefaults standardUserDefaults]
                        stringForKey:@"userId"];
    NSString *post = [NSString stringWithFormat:@"{\"groupId\":\"%@\",\"userId\":\"%@\"}",groupId, userId];
   // NSLog(@"%@",post);
    NSData *postData = [post dataUsingEncoding:NSUTF8StringEncoding];
    NSString *urlStr = [NSString stringWithFormat:@"http://%@:%@/removeUserFromGroup",IPADDRESS, PORT];
    NSMutableURLRequest *request = [[TalkolocoUtils sharedInstance] buildRequest:urlStr withPostData:postData];
    NSOperationQueue *queue = [[NSOperationQueue alloc] init];
    [NSURLConnection sendAsynchronousRequest:request queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
     {
         if ([data length] == 0 || error != nil){
             NSLog(@"Error: %@", [error description]);
         }
         [self performSelectorOnMainThread:@selector(stopLoading) withObject:nil waitUntilDone:NO];
         [self performSelectorOnMainThread:@selector(popBack) withObject:nil waitUntilDone:NO];
     }];
}

-(void)popBack{
    [self.activityIndicator stopAnimating];
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)addUserToGroup{
    [self addUserToGroup: NO];
}

-(void)addUserToGroup:(BOOL)isQuiteMode{
    
    if (!isQuiteMode){
        [[self navigationItem] setRightBarButtonItem:self.activityButton];
        [self.activityIndicator startAnimating];
    }
    
    NSString *userId = [[NSUserDefaults standardUserDefaults]
                        stringForKey:@"userId"];
    NSString *groupId = self.group.groupId;
    NSString *post = [NSString stringWithFormat:@"{\"groupId\":\"%@\",\"userId\":\"%@\"}",groupId, userId];
   // NSLog(@"%@",post);
    NSData *postData = [post dataUsingEncoding:NSUTF8StringEncoding];
    NSString *urlStr = [NSString stringWithFormat:@"http://%@:%@/addUserToGroup",IPADDRESS, PORT];
    NSMutableURLRequest *request = [[TalkolocoUtils sharedInstance]  buildRequest:urlStr withPostData:postData];
    //super.cancelKeyboardNotifications = YES;
    NSOperationQueue *queue = [[NSOperationQueue alloc] init];
    [NSURLConnection sendAsynchronousRequest:request queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
     {
         if ([data length] > 0 && error == nil){
             NSString *str=[[NSString alloc]initWithData:data encoding:NSUTF8StringEncoding];
             NSLog(str, @"%@");
             NSMutableArray *members = [[NSMutableArray alloc] initWithArray:_group.members];
             [members addObject:userId];
             _group.members = [members copy];
             _group.size = [_group.members count];
         } else {
             NSLog(@"Error: %@", [error description]);
         }
         if (!isQuiteMode)
             [self performSelectorOnMainThread:@selector(stopLoading) withObject:nil waitUntilDone:NO];
     }];
}

-(void)loadGroupMessages{
    [self loadGroupMessages:[[NSNumber alloc] initWithBool:YES]];
}


-(void)loadGroupMessages:(NSNumber *)shouldScroll{
    NSString *groupId = self.group.groupId;
    NSString *post = [NSString stringWithFormat:@"{\"groupId\":\"%@\",\"chunk\":\"%@\"}",groupId,[NSString stringWithFormat:@"%i",chunk]];
    //NSLog(@"%@",post);
    NSData *postData = [post dataUsingEncoding:NSUTF8StringEncoding];
    NSString *urlStr = [NSString stringWithFormat:@"http://%@:%@/groups/%@/Messages",IPADDRESS, PORT, groupId];
    NSMutableURLRequest *request = [[TalkolocoUtils sharedInstance]  buildRequest:urlStr withPostData:postData];
    //super.cancelKeyboardNotifications = YES;
    NSOperationQueue *queue = [[NSOperationQueue alloc] init];
    [NSURLConnection sendAsynchronousRequest:request queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
     {
         //super.cancelKeyboardNotifications = NO;
         if ([data length] > 0 && error == nil)
             [self receivedMessagesData:data];
         else {
             NSLog(@"Error: %@", [error description]);
         }
         [self performSelectorOnMainThread:@selector(finishSend:) withObject:shouldScroll waitUntilDone:NO];
     }];

}

- (void)finishSend:(NSNumber *)shouldScroll{
    //[self.activityIndicator stopAnimating];
    [super.tableView reloadData];
    if ([shouldScroll boolValue])
        [super scrollToBottomAnimated:NO];
    
    if ([_messages count]<CHUNK_SIZE){
        moreBtn.hidden = YES;
    }else{
        moreBtn.hidden = NO;
        if (_group.isPrivate){
            container.frame = CGRectMake(0, 0, self.view.frame.size.width, 40);
            super.tableView.tableHeaderView = container;
        } else {
            _mapView.frame = CGRectMake(0, 2, self.view.frame.size.width, 128);
            bottomView.frame = CGRectMake(0, 128, self.view.frame.size.width, 2);
        }
    }
    
    [self stopLoading];
}

#pragma mark - Table view data source
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.messages.count;
}

#pragma mark - Messages view delegate
- (void)sendPressed:(UIButton *)sender withText:(NSString *)text
{
    /*[createArrow removeFromSuperview];
    [inviteBtn removeFromSuperview];*/
    
    if (userLocation!=nil)
        [[TalkolocoUtils sharedInstance] saveUserLocation:userLocation];
    [self.messages addObject:text];
    
    NSDate *date = [NSDate date];
    
    [self.timestamps addObject:date];
    
   // NSLocale* currentLoc = [NSLocale currentLocale];
   // NSLog(@"%@",[date descriptionWithLocale:currentLoc]);

    NSString *userId = [[NSUserDefaults standardUserDefaults]
                        stringForKey:@"userId"];

    NSString *userName = [[NSUserDefaults standardUserDefaults]
                        stringForKey:@"username"];

    [self.senderIds addObject:userId];
    [self.userNames addObject:userName];
    [self.usersIsAtPlace addObject:[NSNumber numberWithBool:_isAtPlace]];
    [self.messagesReceived addObject:[NSNumber numberWithBool:NO]];

    [MessageSoundEffect playMessageSentSound];

    [self postMessage:text withUserId:userId withUserName:userName];

    if (![_group.members containsObject:userId]){
        NSMutableArray *members = [_group.members mutableCopy];
        [members addObject:userId];
        _group.members = [members copy];
    }
}

-(void)postMessage:(NSString *)text withUserId:(NSString *)userId withUserName:(NSString *)userName{
  //  [Flurry logEvent:@"send_message" timed:YES];
    
    //[[TalkolocoUtils sharedInstance] checkBetaStatus:^{
        /*if ([TalkolocoUtils sharedInstance].shouldForceUpgrade){
            [[TalkolocoUtils sharedInstance]  showForceUpgradeMessage];
            return;
        }
        
        if (![TalkolocoUtils sharedInstance].shouldContinueBeta){
            [[TalkolocoUtils sharedInstance]  showBetaClosedMessage];
            return;
        }*/
    //}];
    
    
    [super finishSend];
    
    NSString *groupId = self.group.groupId;
    
    time_t unixTime = (time_t)[[NSDate date] timeIntervalSince1970];
    chunk++;
    
    NSString *post = [NSString stringWithFormat:@"{\"groupId\":\"%@\",\"userId\":\"%@\",\"userName\":\"%@\",\"text\":\"%@\",\"date\":\"%ld\",\"latitude\":\"%f\",\"longitude\":\"%f\",\"chunk\":\"%i\",\"phoneType\":\"%@\"}",groupId, userId, userName, text, unixTime, userLocation.coordinate.latitude, userLocation.coordinate.longitude,chunk, @"iPhone"];
   // NSLog(@"%@",post);
    NSData *postData = [post dataUsingEncoding:NSUTF8StringEncoding];
    NSString *urlStr = [NSString stringWithFormat:@"http://%@:%@/newGroupMessage",IPADDRESS, PORT];
    NSMutableURLRequest *request = [[TalkolocoUtils sharedInstance]  buildRequest:urlStr withPostData:postData];
    
    NSOperationQueue *queue = [[NSOperationQueue alloc] init];
    [NSURLConnection sendAsynchronousRequest:request queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
     {
         if ([data length] > 0 && error == nil)
             [self receivedPostMessageData:data];
         else {
             NSLog(@"Error: %@", [error description]);
         }
         [self performSelectorOnMainThread:@selector(stopLoading) withObject:nil waitUntilDone:NO];
       //  [Flurry endTimedEvent:@"send_message" withParameters:nil];
     }];
    }

-(void)stopLoading{
    
    /*for (NSDate *date in _timestamps) {
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"MMM dd, yyyy HH:mm"];
        NSLog(@"%@",[formatter stringFromDate:date]);
    }*/

    
    [self.tableView reloadData];
    [self.activityIndicator stopAnimating];
    //self.followerLable.text = [NSString stringWithFormat:@"%i", _group.size];
    NSString *userId = [[NSUserDefaults standardUserDefaults] stringForKey:@"userId"];

    if (_group.members==nil || [_group.members count]==0 || [_group.members indexOfObject:userId]== NSNotFound){
        [[self navigationItem] setRightBarButtonItem:nil];
    } else {
        [[self navigationItem] setRightBarButtonItem:self.unFollowButton];
    }
    [_activityIndicator removeFromSuperview];
    
    self.tableView.scrollEnabled = YES;
    moreBtn.userInteractionEnabled = YES;
}

-(void)receivedPostMessageData:(NSData *)data{
    NSString *str=[[NSString alloc]initWithData:data encoding:NSUTF8StringEncoding];
    if ([str containsString:@"Authentication Error"]){
        [self facebookTalkoLogin];
    } else {
       // NSLog(str, @"%@");
        NSArray *jsonArray=[NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
        [self parsePostMessageResponseToMessages:jsonArray];
    }
}

-(void)parsePostMessageResponseToMessages:(NSArray *)jsonArray{

    self.messagesReceived = [NSMutableArray new];
    
    // for (NSString *item in _messages) {
    for (NSUInteger i=0; i<[_messages count]; i++) {
        [self.messagesReceived addObject:[NSNumber numberWithBool:YES]];
    }

}


-(void)receivedMessagesData:(NSData *)data{
    NSString *str=[[NSString alloc]initWithData:data encoding:NSUTF8StringEncoding];
    if ([str containsString:@"Authentication Error"]){
        [self facebookTalkoLogin];
    } else {
        //NSLog(str, @"%@");
        NSArray *jsonArray=[NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
        [self parseResponseToMessages:jsonArray];

    }
    
}

-(void)facebookTalkoLogin{
    
    NSString *deviceToken = [[NSUserDefaults standardUserDefaults] objectForKey: @"deviceToken"];
    NSString *fbToken = [[NSUserDefaults standardUserDefaults] stringForKey:@"fbAccessToken"];
    
    NSString *post = [NSString stringWithFormat:@"{\"token\":\"%@\", \"deviceToken\":\"%@\", \"deviceType\":\"%@\"}", fbToken, deviceToken, @"iPhone"];
    //NSLog(@"%@",post);
    NSData *postData = [post dataUsingEncoding:NSUTF8StringEncoding];
    
    NSString *urlStr = [NSString stringWithFormat:@"http://%@:%@/login/facebookWithDevice",IPADDRESS, PORT];
    NSMutableURLRequest *request = [[TalkolocoUtils sharedInstance] buildRequest:urlStr withPostData:postData];
    
    NSOperationQueue *queue = [[NSOperationQueue alloc] init];
    [NSURLConnection sendAsynchronousRequest:request queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
     {
         [[TalkolocoUtils sharedInstance] parseCookieFromRespose:response];
         
         if ([data length] > 0 && error == nil) {
            // NSString *strUser=[[NSString alloc]initWithData:data encoding:NSUTF8StringEncoding];
          //   NSLog(@"User - %@", strUser);
             NSDictionary *dUser=[NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
             TalkolocoUser *tUser= [[TalkolocoUtils sharedInstance] parseResponseToUser:dUser];
             if (tUser.userName!=nil)
                 [[NSUserDefaults standardUserDefaults] setObject:tUser.userName forKey:@"username"];
             if (tUser.userId!=nil)
                 [[NSUserDefaults standardUserDefaults] setObject:tUser.userId forKey:@"userId"];
             
             [[NSUserDefaults standardUserDefaults] synchronize];
             
             [self performSelectorOnMainThread:@selector(loadGroupMessages) withObject:nil waitUntilDone:NO];
         } else {
             NSLog(@"Error: %@", [error description]);
         }
     }];
}

/*-(void)parseResponseToLastMessage:(NSArray *)jsonArray{
    NSDictionary *item = [jsonArray lastObject];
    [self.messages addObject:[item objectForKey:@"text"]];
    NSString *dateString = [item objectForKey:@"created"];
    double timeIntervalInSecs = [dateString doubleValue]/1000;
    NSDate *date = [NSDate dateWithTimeIntervalSince1970: timeIntervalInSecs];
    [self.timestamps addObject:date];
    NSString *senderId = [item objectForKey:@"senderId"];
    [self.senderIds addObject:senderId];
    NSString *senderName = [item objectForKey:@"senderName"];
    [self.userNames addObject:senderName];
    NSString *senderIsAtPlaceSrt = [[item objectForKey:@"senderIsAtPlace"] stringValue];
    BOOL senderIsAtPlace = [senderIsAtPlaceSrt isEqualToString:@"1"]? YES:NO;
    [self.usersIsAtPlace addObject:[NSNumber numberWithBool:senderIsAtPlace]];
    [self.messagesReceived addObject:[NSNumber numberWithBool:YES]];
}*/

- (void)parseResponseToMessages:(NSArray *)jsonArray{
    self.tableView.scrollEnabled = NO;
    self.messages = [NSMutableArray new];
    self.timestamps = [NSMutableArray new];
    self.senderIds = [NSMutableArray new];
    self.userNames = [NSMutableArray new];
    self.usersIsAtPlace = [NSMutableArray new];
    self.messagesReceived = [NSMutableArray new];
    for (NSDictionary *item in jsonArray) {
        [self.messages addObject:[item objectForKey:@"text"]];        
        NSString *dateString = [item objectForKey:@"created"];
        double timeIntervalInSecs = [dateString doubleValue]/1000;
        NSDate *date = [NSDate dateWithTimeIntervalSince1970: timeIntervalInSecs];
        [self.timestamps addObject:date];
        NSString *senderId = [item objectForKey:@"senderId"];
        [self.senderIds addObject:senderId];
        NSString *senderName = [item objectForKey:@"senderName"];
        [self.userNames addObject:senderName];
        
        NSString *senderIsAtPlaceSrt = [[item objectForKey:@"senderIsAtPlace"] stringValue];
        BOOL senderIsAtPlace = [senderIsAtPlaceSrt isEqualToString:@"1"]? YES:NO;
        [self.usersIsAtPlace addObject:[NSNumber numberWithBool:senderIsAtPlace]];
        [self.messagesReceived addObject:[NSNumber numberWithBool:YES]];
    }
    
   /* if ([_messages count]==0){
        [self performSelectorOnMainThread:@selector(xxx) withObject:nil waitUntilDone:NO];
    }*/
    
    self.tableView.scrollEnabled = YES;

}

/*
-(void)xxx{
    createArrow.hidden = NO;
    inviteBtn.hidden = NO;
    [UIView animateWithDuration:5.0 animations:^{
        createArrow.alpha = 1.0;
    }];
    
    [UIView animateWithDuration:2.0 animations:^{
        inviteBtn.alpha = 1.0;
    }];
}*/

- (BubbleMessageStyle)messageStyleForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *userId = [[NSUserDefaults standardUserDefaults]
                        stringForKey:@"userId"];
    return [userId isEqualToString: [self.senderIds objectAtIndex:indexPath.row]] ?  BubbleMessageStyleOutgoingDefault : BubbleMessageStyleIncomingDefault;
}

- (MessagesViewTimestampPolicy)timestampPolicyForMessagesView
{
    return MessagesViewTimestampPolicyCustom;
}

- (BOOL)hasTimestampForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row==0 ||[_timestamps count]==1)
        return YES;
    
    if (shouldHaveTimestamp==nil){
        [self initShouldHaveTimestamp];
    }
    
    NSNumber *strRow = [[NSNumber alloc] initWithInt: indexPath.row];
    NSNumber *prevStrRow = [[NSNumber alloc] initWithInt: indexPath.row -1];

    HasTimestamp *hasTimestampObj = [shouldHaveTimestamp objectForKey: strRow];
    HasTimestamp *prevHasTimestampObj = [shouldHaveTimestamp objectForKey: prevStrRow];
    
    if (hasTimestampObj==nil){
        hasTimestampObj = [HasTimestamp new];
        
        NSDate *dateA = [_timestamps objectAtIndex:indexPath.row - 1];
        NSDate *dateB = [_timestamps objectAtIndex:indexPath.row];
        
        NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
        
        NSDateComponents *components = [calendar components:NSSecondCalendarUnit
                                                   fromDate:dateA
                                                     toDate:dateB
                                                    options:0];
        
        hasTimestampObj.deltaInSeconds = components.second;
        hasTimestampObj.sumDeltaInSeconds = prevHasTimestampObj.sumDeltaInSeconds + components.second;
        hasTimestampObj.hasTimestamp = NO;
        
        if (hasTimestampObj.sumDeltaInSeconds > 120){
            hasTimestampObj.hasTimestamp = YES;
            hasTimestampObj.sumDeltaInSeconds = 0;
        }
        
        [shouldHaveTimestamp setObject:hasTimestampObj forKey:strRow];
    }

    
    BOOL retValue = hasTimestampObj.hasTimestamp;
    
    return retValue;
}

-(void)initShouldHaveTimestamp {
    
    shouldHaveTimestamp = [NSMutableDictionary new];
    
    HasTimestamp *firstHObj = [HasTimestamp new];
    firstHObj.hasTimestamp = YES;
    firstHObj.deltaInSeconds = 0;
    firstHObj.sumDeltaInSeconds = 0;
    
    NSNumber *firstRow = [[NSNumber alloc] initWithInt: 0];

    [shouldHaveTimestamp setObject:firstHObj forKey:firstRow];
    
    for (NSInteger i = 1; i < [_timestamps count]; i++) {
        NSDate *dateA = [_timestamps objectAtIndex:i-1];
        NSDate *dateB = [_timestamps objectAtIndex:i];
        
        NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
        
        NSDateComponents *components = [calendar components:NSSecondCalendarUnit
                                                   fromDate:dateA
                                                     toDate:dateB
                                                    options:0];
        
       // NSLog(@"Delta in Seconds: (%i)%i",i, components.second);
        
        NSNumber *strRow = [[NSNumber alloc] initWithInt: i];
        NSNumber *prevStrRow = [[NSNumber alloc] initWithInt: i- 1];
        
        HasTimestamp *hasTimestampObj = [shouldHaveTimestamp objectForKey: strRow];
        HasTimestamp *prevHasTimestampObj = [shouldHaveTimestamp objectForKey: prevStrRow];
        
        if (hasTimestampObj==nil){
            hasTimestampObj = [HasTimestamp new];
            hasTimestampObj.deltaInSeconds = components.second;
            hasTimestampObj.sumDeltaInSeconds = prevHasTimestampObj.sumDeltaInSeconds + components.second;
            hasTimestampObj.hasTimestamp = NO;
            
            if (hasTimestampObj.sumDeltaInSeconds > 120){
                hasTimestampObj.hasTimestamp = YES;
                hasTimestampObj.sumDeltaInSeconds = 0;
            }
            
            [shouldHaveTimestamp setObject:hasTimestampObj forKey:strRow];
        }

    }
}

#pragma mark - Messages view data source
- (NSString *)textForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return [self.messages objectAtIndex:indexPath.row];
}

- (NSDate *)timestampForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return [self.timestamps objectAtIndex:indexPath.row];
}

- (NSString *)userNameForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return [self.userNames objectAtIndex:indexPath.row];
}

- (BOOL)userAtPlaceAtIndexPath:(NSIndexPath *)indexPath
{
    return [[self.usersIsAtPlace objectAtIndex:indexPath.row] boolValue];
}

- (BOOL)messageReceivedAtPlaceAtIndexPath:(NSIndexPath *)indexPath
{
    return [[self.messagesReceived objectAtIndex:indexPath.row] boolValue];
}

- (NSString *)senderIdForRowAtIndexPath:(NSIndexPath *)indexPath{
    return [self.senderIds objectAtIndex:indexPath.row];
}



@end