#import <UIKit/UIKit.h>
#import "ChatTextView.h"

@interface MessageInputView : UIImageView <NSLayoutManagerDelegate>

@property (strong, nonatomic) ChatTextView *textView;
@property (strong, nonatomic) UIButton *sendButton;

#pragma mark - Initialization
- (id)initWithFrame:(CGRect)frame
           delegate:(id<UITextViewDelegate>)delegate;

#pragma mark - Message input view
- (void)adjustTextViewHeightBy:(CGFloat)changeInHeight;
+ (CGFloat)textViewLineHeight;
+ (CGFloat)maxLines;
+ (CGFloat)maxHeight;

@end