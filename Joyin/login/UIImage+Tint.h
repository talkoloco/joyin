//
//  UIImage+Tint.h
//  Talkoloco
//
//  Created by Ohad on 2013-09-08.
//  Copyright (c) 2013 Talkoloco. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (Tint)

+ (UIImage *)imageWithColor:(UIColor *)color;

@end
