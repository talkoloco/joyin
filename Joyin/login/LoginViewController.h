//
//  LoginViewController.h
//  Talkoloco
//
//  Created by Ohad on 2013-09-28.
//  Copyright (c) 2013 Talkoloco. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <FacebookSDK/FacebookSDK.h>

@protocol FacebookLoginDelegate<NSObject>

@optional

-(void)onLoginComplete;
-(void)onLogoutComplete;

@end

@interface LoginViewController : UIViewController <UIAlertViewDelegate>

-(void)start;
-(void)facebookLogin;
-(void)facebookTalkoLogin:(NSString *)fbToken withDeviceToken:(NSString *)deviceToken;

@end
