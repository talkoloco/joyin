#import "LoginViewController.h"
#import "IntroControll.h"
#import "UIImage+Tint.h"
#import "TalkolocoUtils.h"
#import "AppDelegate.h"
#import "TalkolocoUser.h"
//#import "Flurry.h"
#import "DejalActivityView.h"
#import "NSString+JRStringAdditions.h"


@implementation LoginViewController{
    NSUInteger retryCount;
}

- (id)init
{
    self = [super initWithNibName:nil bundle:nil];
    self.modalPresentationStyle = UIModalPresentationFullScreen;
    //[[TalkolocoUtils sharedInstance ] checkBetaStatus:^{}];
    retryCount = 0;
    return self;
}

- (void) loadView {
    [super loadView];
    IntroModel *model1 = [[IntroModel alloc] initWithTitle:@"Sign In" description:@"We'll never post anything on your behalf." imageAsText:@"SignIn"];
    IntroModel *model2 = [[IntroModel alloc] initWithTitle:@"Community" description:@"Increase your favorite place engagement." imageAsText:@"love"];
    IntroModel *model3 = [[IntroModel alloc] initWithTitle:@"It's Safe" description:@"We respect your privacy!" imageAsText:@"bw_smile"];

   // IntroModel *model3 = [[IntroModel alloc] initWithTitle:@"Talkoloco 3" description:@"Talkoloco gives..." imageAsText:@"image3.jpg"];
    self.view = [[IntroControll alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height) pages:@[model1, model2,model3] context: self];
    
}

-(void)start{
    
    //[[TalkolocoUtils sharedInstance] checkBetaStatus:^{}];
    

    UIStoryboard *sb = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    UITabBarController *centerViewController = [sb instantiateViewControllerWithIdentifier:@"talkoTabBar"];
    centerViewController.selectedViewController = [centerViewController.viewControllers objectAtIndex:0];
    [self presentViewController:centerViewController animated: YES completion:nil];
    
    [DejalBezelActivityView removeViewAnimated:YES];
}

-(void)facebookLogin{
    [self facebookLogin:YES];
}

-(void)facebookLogin:(BOOL)allowLoginUI{
    
    //[FBSession.activeSession closeAndClearTokenInformation];
    // get the app delegate so that we can access the session property
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];

    [DejalBezelActivityView activityViewForView:appDelegate.window];
    
    //[[TalkolocoUtils sharedInstance] facebookLogin];
    [FBSession openActiveSessionWithReadPermissions:@[@"basic_info", @"email" , @"user_friends", @"user_photos", @"friends_photos"]
                                           allowLoginUI:YES
                                      completionHandler:^(FBSession *session,
                                    FBSessionState status,
                                    NSError *error) {
                                          
        if (error){
            [DejalBezelActivityView removeViewAnimated:YES];
            NSLog(@"%@",[error description]);
        } else if (status == FBSessionStateOpen) {
            //FBSession *fbSession  = FBSession.activeSession;
            
            FBAccessTokenData *ssn = [FBSession.activeSession accessTokenData];
            NSString *fbAccessToken = [ssn accessToken];
            /*
            NSDate *exDate = [ssn expirationDate];
            NSDate *refreshDate = [ssn refreshDate];
            NSDate *pRefDate = [ssn permissionsRefreshDate];
            NSArray *pArr = [ssn permissions];*/

            [[NSUserDefaults standardUserDefaults] setObject:fbAccessToken forKey:@"fbAccessToken"];
            NSString *deviceToken = [[NSUserDefaults standardUserDefaults] objectForKey:@"deviceToken"];
            [self facebookTalkoLogin:fbAccessToken withDeviceToken:deviceToken];
        } else if (status == FBSessionStateClosed) {
            [[NSUserDefaults standardUserDefaults] setObject:nil forKey:@"fbAccessToken"];
            [[NSUserDefaults standardUserDefaults] synchronize];
        }
  
    }];
}

-(void)invalidFacebookLogin{
    [FBSession.activeSession  closeAndClearTokenInformation];
    FBSession.activeSession=nil;
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Invalid Login"
                                                    message:@"Something went wrong with your login"
                                                   delegate:nil
                                          cancelButtonTitle:@"OK"
                                          otherButtonTitles:nil];
    [alert show];
    [DejalBezelActivityView removeViewAnimated:YES];

}

-(void)facebookTalkoLogin:(NSString *)fbToken withDeviceToken:(NSString *)deviceToken {
    
    NSString *post = [NSString stringWithFormat:@"{\"token\":\"%@\", \"deviceToken\":\"%@\", \"deviceType\":\"%@\"}", fbToken, deviceToken, @"iPhone"];
    NSLog(@"%@",post);
    NSData *postData = [post dataUsingEncoding:NSUTF8StringEncoding];
    
    NSString *urlStr = [NSString stringWithFormat:@"http://%@:%@/login/facebookWithDevice",IPADDRESS, PORT];
    NSMutableURLRequest *request = [[TalkolocoUtils sharedInstance] buildRequest:urlStr withPostData:postData];
    
    NSOperationQueue *queue = [[NSOperationQueue alloc] init];
    [NSURLConnection sendAsynchronousRequest:request queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
     {
         [[TalkolocoUtils sharedInstance] parseCookieFromRespose:response];
         
         if ([data length] > 0 && error == nil) {
             NSString *strUser=[[NSString alloc]initWithData:data encoding:NSUTF8StringEncoding];
             NSLog(@"User - %@", strUser);
             
             if (retryCount  < 6  && [strUser containsString:@"Login is not valid"]){
                 retryCount++;
                 [self performSelectorOnMainThread:@selector(closeFacebookSessionAndLogin) withObject:nil waitUntilDone:NO];
             } else if (retryCount == 6 && [strUser containsString:@"Login is not valid"]){
                 [self performSelectorOnMainThread:@selector(invalidFacebookLogin) withObject:nil waitUntilDone:NO];
             } else {
                 NSDictionary *dUser=[NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                 TalkolocoUser *tUser= [[TalkolocoUtils sharedInstance] parseResponseToUser:dUser];
                 [[TalkolocoUtils sharedInstance] updateUserDefaultsWithUser:tUser];
                 [self performSelectorOnMainThread:@selector(start) withObject:nil waitUntilDone:NO];
             }
         } else {
             NSLog(@"Error: %@", [error description]);
         }
     }];
}

-(void)closeFacebookSessionAndLogin{
    FBSession* session = [FBSession activeSession];
    [session closeAndClearTokenInformation];
    [session close];
    [FBSession setActiveSession:nil];
    [FBSession.activeSession closeAndClearTokenInformation];
    [FBSession.activeSession close];
    [FBSession setActiveSession:nil];
    
    NSHTTPCookieStorage* cookies = [NSHTTPCookieStorage sharedHTTPCookieStorage];
    for (NSHTTPCookie* cookie in
         [[NSHTTPCookieStorage sharedHTTPCookieStorage] cookies]) {
        [cookies deleteCookie:cookie];
    }
    
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"fbAccessToken"];
    [self facebookLogin:YES];
}

-(void)saveUserFacebookDetails:(TalkolocoUser *)user{
    
    NSString *post = [NSString stringWithFormat:@"{\"username\":\"%@\",\"token\":\"%@\",\"first_name\":\"%@\",\"last_name\":\"%@\",\"facebook_id\":\"%@\",\"email\":\"%@\"}", user.userName, user.fbAccessToken, user.first_name, user.last_name, user.userId, user.email];
    NSLog(@"%@",post);
    NSData *postData = [post dataUsingEncoding:NSUTF8StringEncoding];
    
    NSString *urlStr = [NSString stringWithFormat:@"http://%@:%@/facebookLogin",IPADDRESS, PORT];
    
    NSMutableURLRequest *request = [[TalkolocoUtils sharedInstance]  buildRequest:urlStr withPostData:postData];
    
    NSOperationQueue *queue = [[NSOperationQueue alloc] init];
    [NSURLConnection sendAsynchronousRequest:request queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error){
        BOOL success = NO;
        if ([data length] > 0 && error == nil){
            success = [self receiveData:data];
        }
        else {
            NSLog(@"Error: %@", [error description]);
        }
        if (success){
            
        }
     //   [Flurry endTimedEvent:@"save_settings" withParameters:nil];
        
    }];

}

-(BOOL)receiveData:(NSData *)data{
    return YES;
}


@end
