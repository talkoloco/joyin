//
//  NSString+JRStringAdditions.m
//  Talkoloco
//
//  Created by Ohad on 2013-11-08.
//  Copyright (c) 2013 Talkoloco. All rights reserved.
//

#import "NSString+JRStringAdditions.h"

@implementation NSString (JRStringAdditions)

- (BOOL)containsString:(NSString *)string
               options:(NSStringCompareOptions)options {
    NSRange rng = [self rangeOfString:string options:options];
    return rng.location != NSNotFound;
}

- (BOOL)containsString:(NSString *)string {
    return [self containsString:string options:0];
}

@end