//
//  UIView+Expanded.h
//  Talkoloco
//
//  Created by Ohad on 2014-03-30.
//  Copyright (c) 2014 Talkoloco. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (Expanded)

-(void)resizeToFitSubviews;

@end
