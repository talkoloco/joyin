//
//  NSString+JRStringAdditions.h
//  Talkoloco
//
//  Created by Ohad on 2013-11-08.
//  Copyright (c) 2013 Talkoloco. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (JRStringAdditions)

- (BOOL)containsString:(NSString *)string;
- (BOOL)containsString:(NSString *)string
               options:(NSStringCompareOptions)options;

@end