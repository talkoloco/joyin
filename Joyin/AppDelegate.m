//
//  OhadAppDelegate.m
//  TabCont
//
//  Created by Ohad on 2013-04-22.
//  Copyright (c) 2013 Ohad. All rights reserved.
//

#import "AppDelegate.h"
#import "TalkolocoUtils.h"
#import "ChatViewController.h"
#import "TalkolocoUser.h"
//#import "Flurry.h"
//#import "TestFlight.h"
#import "HintView.h"
#import "NSString+JRStringAdditions.h"
#import "Joyin-Swift.h"

@interface AppDelegate (){
    NSString *groupID;
    NSString *senderName;
    NSString *groupName;
    NSString *incommingMessage;
    NSDictionary *cutomerUserInfo;
}
//@property (nonatomic, strong) IBOutlet UINavigationController *myNavController;

@end

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions{
    
    //testSwift *tt = [testSwift newInstanceNamed:@"asd"];

    //NSString *appDomain = [[NSBundle mainBundle] bundleIdentifier];
    //[[NSUserDefaults standardUserDefaults] removePersistentDomainForName:appDomain];
    
    if (launchOptions==nil){
        
        [self setup];
        NSString *fbAccessToken = [[NSUserDefaults standardUserDefaults] stringForKey:@"fbAccessToken"];
        
        //NSDate *fbExpirationDate =  [[NSUserDefaults standardUserDefaults] objectForKey: @"fbExpirationDate"];
        
        fbAccessToken = nil;
        
        if (fbAccessToken!=nil){
            [self navigateToMyPotentialMatches];
        }
        return YES;
        
    } else { //if (launchOptions!=nil) received push notification
        
        UIApplicationState state = [[UIApplication sharedApplication] applicationState];
        
        NSString *chatVisible = [[NSUserDefaults standardUserDefaults] objectForKey: @"ChatVisible"];
        NSString *chatGroupId = [[NSUserDefaults standardUserDefaults] objectForKey: @"ChatGroupId"];
        
        NSDictionary* userInfo = [launchOptions objectForKey:UIApplicationLaunchOptionsRemoteNotificationKey];
        
        groupID = [userInfo objectForKey:@"groupId"];
        if (groupID==nil) groupID = [launchOptions objectForKey:@"groupId"];
        if (groupID==nil) {
            [self navigateToMyPotentialMatches];
            return YES;
        }
        senderName= [launchOptions objectForKey:@"senderName"];
        groupName = [launchOptions objectForKey:@"groupName"];
        
        if (state == UIApplicationStateInactive){
            // use NSApplicationLaunchRemoteNotificationKey
            if ([chatVisible isEqualToString:@"YES"] && [chatGroupId isEqualToString:groupID]){
                [self postRemoteNotification];
                return YES;
            } else {
                
                NSString *token = [[NSUserDefaults standardUserDefaults] objectForKey:@"fbAccessToken"];
                
                if (token==nil || [@"" isEqualToString:token]){
                    [self handleRequestWhenLoggedout:userInfo withAction:@selector(onLoginComplete)];
                } else {
                    UIStoryboard *sb = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                    UITabBarController *centerViewController = [sb instantiateViewControllerWithIdentifier:@"talkoTabBar"];
                    
                    [centerViewController.delegate tabBarController:centerViewController shouldSelectViewController:[[centerViewController viewControllers] objectAtIndex:0]];
                    [centerViewController setSelectedIndex:0];
                    
                    self.window.rootViewController = centerViewController;
                    
                    UINavigationController *navController = (UINavigationController *)centerViewController.selectedViewController;
                    ChatViewController *cvc = [[ChatViewController alloc] initByPush];
                    //cvc.hidesBottomBarWhenPushed = YES;
                    cvc.group = [Group new];
                    cvc.group.groupId = groupID;
                
                    [navController pushViewController:cvc animated:YES];
                }

                
                
                //return YES;
            }
        } else if (state == UIApplicationStateActive){
            //NSLog(@"groupID: %@",groupID);
            //NSLog(@"chatGroupId: %@",chatGroupId);
            if (![@"" isEqualToString:senderName] && [chatVisible isEqualToString:@"YES"] && [groupID isEqualToString:chatGroupId]){
                [self postRemoteNotification];
                return YES;
            } else {
                [MessageSoundEffect playMessageReceivedSound];
            }
            
            NSString *message = [launchOptions objectForKey:@"alert"];
            incommingMessage = [NSString stringWithFormat:@"%@@%@: %@",senderName, groupName, message];
            [self.hintView show:5];
            
            return YES;
        }
        
    }
    return YES;
}

-(void)alert:(NSString *)text{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Talkoloco"
                                                    message:text
                                                   delegate:self
                                          cancelButtonTitle:@"OK"
                                          otherButtonTitles:nil];
    [alert show];
}

-(void)navigateToMyPotentialMatches{
    UIStoryboard *sb = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    UITabBarController *centerViewController = [sb instantiateViewControllerWithIdentifier:@"talkoTabBar"];
    self.window.rootViewController = centerViewController;
}

-(NSString*)titleforHintView:(HintView*)hintView{
    return incommingMessage;
}

-(BOOL)application:(UIApplication *)application
           openURL:(NSURL *)url
 sourceApplication:(NSString *)sourceApplication
        annotation:(id)annotation {
    //@"com.apple.mobilesafari"
    //@"com.facebook.Facebook"
    NSString *urlStr = [url description];
    if ([urlStr containsString:@"talkoloco"]){
        return [self handleCustomerURL:[url query]];
    } else {
        return [[FBSession activeSession] handleOpenURL:url];
    }
    
}

-(BOOL)handleCustomerURL:(NSString *)query{
    NSArray *components = [query componentsSeparatedByString:@"&"];
    NSMutableDictionary *userInfo = [[NSMutableDictionary alloc] init];
    for (NSString *component in components) {
        NSArray *subcomponents = [component componentsSeparatedByString:@"="];
        [userInfo setObject:[[subcomponents objectAtIndex:1] stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding]
                       forKey:[[subcomponents objectAtIndex:0] stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
    }
    
    NSString *token = [[NSUserDefaults standardUserDefaults] objectForKey:@"fbAccessToken"];
    
    cutomerUserInfo = userInfo;

    if (token==nil || [@"" isEqualToString:token]){
        [self handleRequestWhenLoggedout:userInfo withAction:@selector(afterExternalInvocation)];
    } else {
        [self afterExternalInvocation];
    }
    return  YES;
}

-(void)postRemoteNotification{
    [[NSNotificationCenter defaultCenter]
     postNotificationName:@"RemotePushNotification"
     object:self];
}

-(void)setup{
   // [[TalkolocoUtils sharedInstance] checkBetaStatus:^{}];
    //[Flurry startSession:@"GYQKGQ3M4MGC25D6JZVH"];
    //[TestFlight takeOff:@"1b9004b3-5770-41f8-9714-5153704071c3"];
    [UIApplication sharedApplication].applicationIconBadgeNumber = 0;
    self.window.backgroundColor = [UIColor whiteColor];
    [self.window makeKeyAndVisible];
    // Let the device know we want to receive push notifications
    [[UIApplication sharedApplication] registerForRemoteNotifications];
    [[NSUserDefaults standardUserDefaults] setObject:@"NO" forKey:@"ChatVisible"];
    [[NSUserDefaults standardUserDefaults] setObject:@"None" forKey:@"ChatGroupId"];
    [[NSUserDefaults standardUserDefaults] setObject:[NSNumber numberWithDouble:0.0095] forKey:@"radius"];

}


-(void)silentLogin: (NSData *)deviceToken{
    NSString *token = nil;
    if (deviceToken==nil)
        token = @"Simulator";
    else{
        token = [deviceToken description];
        token = [token stringByReplacingOccurrencesOfString:@" " withString:@""];
        token = [token stringByReplacingOccurrencesOfString:@"<" withString:@""];
        token = [token stringByReplacingOccurrencesOfString:@">" withString:@""];
    }
    [[NSUserDefaults standardUserDefaults] setObject:token forKey:@"deviceToken"];
}

-(void)buildUserDefaults:(TalkolocoUser *)user{
   // [[NSUserDefaults standardUserDefaults] setObject:user.userName forKey:@"userName"];
    [[NSUserDefaults standardUserDefaults] setObject:@"NO" forKey:@"ChatVisible"];
    [[NSUserDefaults standardUserDefaults] setObject:[NSNumber numberWithDouble:0.0095] forKey:@"radius"];
    //[[NSUserDefaults standardUserDefaults] setObject:user.userId forKey:@"userId"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (void)application:(UIApplication*)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData*)deviceToken
{
	NSLog(@"My token is: %@", deviceToken);
    NSString *userId = [[NSUserDefaults standardUserDefaults]
                        stringForKey:@"userId"];
    NSLog(@"User ID for Device: %@", userId);
    NSString *tokenStr = [deviceToken description];
    tokenStr = [tokenStr stringByReplacingOccurrencesOfString:@" " withString:@""];
    tokenStr = [tokenStr stringByReplacingOccurrencesOfString:@"<" withString:@""];
    tokenStr = [tokenStr stringByReplacingOccurrencesOfString:@">" withString:@""];
    //[Flurry setPushToken:tokenStr];
    [self silentLogin: deviceToken];
    
}

- (void)application:(UIApplication*)application didFailToRegisterForRemoteNotificationsWithError:(NSError*)error
{
	NSLog(@"Failed to get token, error: %@", error);
    NSString *userId = [[NSUserDefaults standardUserDefaults]
                        stringForKey:@"userId"];
    NSLog(@"User ID for Simulator (probably): %@", userId);
    [self silentLogin: nil];
}


- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo{
    NSMutableDictionary *userInfoDic = [NSMutableDictionary new];
    
    NSString *fGroupID = [userInfo objectForKey:@"groupId"];
    NSString *fSenderName= [userInfo objectForKey:@"senderName"];
    NSString *fGroupName = [userInfo objectForKey:@"groupName"];
    NSDictionary *aps = [userInfo objectForKey:@"aps"];
    NSString *message = [aps objectForKey:@"alert"];
    
    if (fGroupID!=nil)
        [userInfoDic setObject:fGroupID forKey:@"groupId" ];
    
    if (fSenderName!=nil)
        [userInfoDic setObject:fSenderName forKey:@"senderName"];
    
    if (fGroupName!=nil)
        [userInfoDic setObject:fGroupName forKey:@"groupName"];
    
    if (message!=nil)
        [userInfoDic setObject:message forKey:@"message"];
    
    NSString *token = [[NSUserDefaults standardUserDefaults] objectForKey:@"fbAccessToken"];

    if (token==nil || [@"" isEqualToString:token]){
        [self handleRequestWhenLoggedout:userInfo withAction:@selector(onLoginComplete)];
    } else {
        [self application:[UIApplication sharedApplication] didFinishLaunchingWithOptions:userInfo];
    }
    NSLog(@"push notification received");
}

-(void)handleRequestWhenLoggedout:(NSDictionary *)userInfo withAction:(SEL)completionAction{
    [FBSession openActiveSessionWithReadPermissions:@[@"basic_info", @"email"]
                                       allowLoginUI:YES
                                  completionHandler:^(FBSession *session,
                                                      FBSessionState status,
                                                      NSError *error) {
                                      
                                      if (error){
                                          //[self navigateHome];
                                          NSLog(@"%@",[error description]);
                                      } else {
                                      
                                          NSString *fbAccessToken = [[FBSession.activeSession accessTokenData] accessToken];
                                          
                                          if (fbAccessToken!=nil){
                                              [[NSUserDefaults standardUserDefaults] setObject:fbAccessToken forKey:@"fbAccessToken"];
                                              
                                              [[TalkolocoUtils sharedInstance] facebookTalkoLogin:^{
                                                  if ([self respondsToSelector:completionAction])
                                                       [self performSelectorOnMainThread:completionAction withObject:nil waitUntilDone:NO];
                                                  
                                              }];
                                          }
                                      }
                                      
                                  }];
}

-(void)navigateHome{
    UIStoryboard *sb = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    UITabBarController *centerViewController = [sb instantiateViewControllerWithIdentifier:@"talkoTabBar"];
    
    [centerViewController.delegate tabBarController:centerViewController shouldSelectViewController:[[centerViewController viewControllers] objectAtIndex:0]];
    [centerViewController setSelectedIndex:0];
    self.window.rootViewController = centerViewController;

}

-(void)onLoginComplete{
    [self navigateHome];
    [self application:[UIApplication sharedApplication] didFinishLaunchingWithOptions:cutomerUserInfo];
}

-(void)afterExternalInvocation{

    NSString *groupId = [cutomerUserInfo objectForKey:@"groupId"];
    NSString *userId = [[NSUserDefaults standardUserDefaults] objectForKey:@"userId"];

    [[TalkolocoUtils sharedInstance] inviteWithUserId:userId toGroup:groupId onView:self.window withCompletionHandler:^{
        [self postRefreshRequest];
    }];
}

-(void)postRefreshRequest{
    [[NSNotificationCenter defaultCenter]
     postNotificationName:@"RefreshRequest"
     object:self];
}

/*
- (void)applicationWillResignActive:(UIApplication *)application{
    
}*/

-(HintView *)hintView{
    if (!_hintView){
        
        _hintView = [[HintView alloc] initWithFrame:CGRectMake(0, 0, self.window.frame.size.width, 80)];
        _hintView.backgroundColor = [UIColor darkGrayColor];
        
        _hintView.dataSource = self;
        _hintView.delegate = self;
        
        UITapGestureRecognizer *singleFingerTap =
        [[UITapGestureRecognizer alloc] initWithTarget:self
                                                action:@selector(hintMessagePressed:)];
        [_hintView addGestureRecognizer:singleFingerTap];
        

    }
    [self.window addSubview:_hintView];

//    [self.window bringSubviewToFront:_hintView];
    

    return _hintView;
}

- (void)hintMessagePressed:(UITapGestureRecognizer *)recognizer {
    UIStoryboard *sb = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    ChatViewController *cvc = [[ChatViewController alloc] initByPush];
    cvc.group = [Group new];
    cvc.group.groupId = groupID;
    UITabBarController *centerViewController = [sb instantiateViewControllerWithIdentifier:@"talkoTabBar"];
    self.window.rootViewController = centerViewController;
    UINavigationController *navController = (UINavigationController *)centerViewController.selectedViewController;
    [navController pushViewController:cvc animated:YES];
}

-(void)validateFacebookToken:(TalkolocoUser *)user{
    
    NSLog(@"%@",[FBSession.activeSession description]);
    
    [FBSession openActiveSessionWithReadPermissions:@[@"basic_info", @"email"]
                                       allowLoginUI:NO
                                  completionHandler:^(FBSession *session,
                                                      FBSessionState status,
                                                      NSError *error) {
                                      
            NSString *fbSessionAccessToken = [[FBSession.activeSession accessTokenData] accessToken];
            NSString *fbUserAcccessToken = [[NSUserDefaults standardUserDefaults] stringForKey:@"fbAccessToken"];
                                    
            if (fbSessionAccessToken==nil || ![fbSessionAccessToken isEqualToString:fbUserAcccessToken]){
                [[NSUserDefaults standardUserDefaults] setObject:fbSessionAccessToken forKey:@"fbAccessToken"];
                user.fbAccessToken = fbSessionAccessToken;
                [self facebookTalkoLogin:fbSessionAccessToken withDeviceToken:user.deviceToken];
            }
                                                                          
            [self buildUserDefaults:user];
        }];
    
}

-(void)facebookTalkoLogin:(NSString *)fbToken withDeviceToken:(NSString *)deviceToken {
    
    NSString *post = [NSString stringWithFormat:@"{\"token\":\"%@\", \"deviceToken\":\"%@\", \"deviceType\":\"%@\"}", fbToken, deviceToken, @"iPhone"];
    NSLog(@"%@",post);
    NSData *postData = [post dataUsingEncoding:NSUTF8StringEncoding];
    
    NSString *urlStr = [NSString stringWithFormat:@"http://%@:%@/login/facebookWithDevice",IPADDRESS, PORT];
    NSMutableURLRequest *request = [[TalkolocoUtils sharedInstance] buildRequest:urlStr withPostData:postData];
    
    NSOperationQueue *queue = [[NSOperationQueue alloc] init];
    [NSURLConnection sendAsynchronousRequest:request queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
     {
         if ([data length] > 0 && error == nil) {
             NSString *strUser=[[NSString alloc]initWithData:data encoding:NSUTF8StringEncoding];
             NSLog(@"User - %@", strUser);
         } else {
             NSLog(@"Error: %@", [error description]);
         }
     }];
}
                       

                       


@end
