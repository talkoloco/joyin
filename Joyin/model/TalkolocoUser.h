//
//  TalkolocoUser.h
//  Talkoloco
//
//  Created by Ohad on 2013-08-30.
//  Copyright (c) 2013 Talkoloco. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TalkolocoUser : NSObject

@property (nonatomic, strong) NSString *userId;
@property (nonatomic, strong) NSString *userName;
@property (nonatomic, strong) NSString *deviceToken;
@property (nonatomic, strong) NSString *email;
@property (nonatomic, strong) NSString *first_name;
@property (nonatomic, strong) NSString *last_name;
@property (nonatomic, strong) NSString *fbAccessToken;
@property (nonatomic, strong) NSString *facebookID;
@property (nonatomic, strong) NSString *status;
@property (nonatomic, strong) NSString *picURL;
@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSArray *privateGroups;
@property (nonatomic) NSUInteger mutual_friend_count;
@property (nonatomic) BOOL selected;
@property (nonatomic, strong) NSDictionary *placeOfInterest;
@property (nonatomic) BOOL isNearBy;


@end