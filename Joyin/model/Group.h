//
//  GroupRequest.h
//  Talkoloco
//
//  Created by Ohad on 2013-08-17.
//  Copyright (c) 2013 Talkoloco. All rights reserved.
//
#import <Foundation/Foundation.h>


@interface Group : NSObject

@property (nonatomic, strong) NSString *groupName;
@property (nonatomic, strong) NSString *groupId;
@property (nonatomic, strong) NSString *creatorId;
@property (nonatomic, strong) NSString *creatorName;
@property (nonatomic, strong) NSArray *members;
@property (nonatomic, strong) NSString *placeName;
@property (nonatomic, strong) NSString *placePicURL;
@property (nonatomic, readwrite) double latitude;
@property (nonatomic, readwrite) double longitude;
@property (nonatomic, strong) NSDate *created;
@property (nonatomic, strong) NSString *message;
@property (nonatomic, readwrite) NSUInteger size;
@property (nonatomic, strong) NSString *address;
@property (nonatomic, readwrite) BOOL someoneAtPlace;
@property (nonatomic, strong) NSString *lastMessage;
@property (nonatomic, readwrite) BOOL isPrivate;



@end
