//
//  AppDelegate.h
//  Talkoloco
//
//  Created by Ohad on 2013-07-27.
//  Copyright (c) 2013 Talkoloco. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <FacebookSDK/FacebookSDK.h>
#import "Group.h"
#import "HintView.h"
#import <FacebookSDK/FacebookSDK.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate,HintViewDataSource, HintViewDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) HintView *hintView;

@end
